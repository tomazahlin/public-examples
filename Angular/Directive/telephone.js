'use strict';

/**
 * @ngdoc directive
 * @name simpleRepairApp.directive:telephoneValidator
 * @description
 */
angular.module('simpleRepairApp')
  .directive('telephoneValidator', function() {
    return {
      restrict: 'A',
      require : 'ngModel',
      link : function(scope, element, attrs, ngModel) {

        var regex = /^[+0-9][-/ 0-9]+$/;

        ngModel.$parsers.push(function(value) {

          // Optional field
          if(!value || value.length == 0)
          {
            ngModel.$setValidity('regex', true);
            return value;
          }

          var valid = regex.test(value);
          ngModel.$setValidity('regex', valid);

          return valid ? value : undefined;

        });

        ngModel.$formatters.push(function(value) {

          ngModel.$setValidity('regex', true);
          return value;

        });

      }
    }
  });