'use strict';

/**
 * @ngdoc directive
 * @name simpleRepairApp.directive:luhnValidator
 * @description
 */
angular.module('simpleRepairApp')
  .directive('luhnValidator', function() {

    var onlyDigits = function(value)
    {
      if(value === undefined) return undefined;
      return value.replace(/\D/g, "");
    };

    return {
      restrict: 'A',
      require : 'ngModel',
      link : function($scope, element, attrs, ngModel) {

        ngModel.$parsers.push(function(value) {

          value = onlyDigits(value);

          var luhnValid = luhn(value);
          ngModel.$setValidity('luhn', luhnValid);

          return luhnValid ? value : undefined;

        });

        ngModel.$formatters.push(function(value) {

          ngModel.$setValidity('luhn', true);

          return value;

        });

      }
    }
  });