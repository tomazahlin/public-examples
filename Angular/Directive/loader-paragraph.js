'use strict';

/**
 * @ngdoc directive
 * @name simpleRepairApp.directive:loaderParagraph
 * @description
 */
angular.module('simpleRepairApp')
  .directive('loaderParagraph', function() {
    return {
      restrict: 'E',
      replace: true,
      template: '<p class="text-center">' +
                  '<span class="glyphicon glyphicon-refresh fa-3x fa-spin"></span> <span translate>Loading</span>...' +
                '</p>'
    };
  });
