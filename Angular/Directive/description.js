'use strict';

/**
 * @ngdoc directive
 * @name simpleRepairApp.directive:descriptionField
 * @description Only one of these allowed per form. If needed, make nested forms with ng-form.
 */
angular.module('simpleRepairApp')
  .directive('descriptionField', function (gettext) {
    return {
      restrict: 'E',
      require: ['form', 'ngModel'],
      scope: {
        'form': '=form',
        'model': '=ngModel',
        'labelClass': '@',
        'inputClass': '@',
        'divClass': '@',
        'textareaClass': '@',
        'labelText': '@',
        'showLabel': '@',
        'showPlaceholder': '@',
        'placeholderText': '@',
        'n': '@',
        'required': '@',
        'messageRequired': '@',
        'messageShort': '@',
        'messageLong': '@'
      },
      compile: function(element, attrs)
      {
        if (!attrs.labelClass) { attrs.labelClass = 'col-sm-4'; }
        if (!attrs.inputClass) { attrs.inputClass = 'col-sm-8'; }
        if (!attrs.divClass) { attrs.divClass = 'form-group'; }
        if (!attrs.textareaClass) { attrs.textareaClass = 'normal full'; }

        if (!attrs.labelText) { attrs.labelText = gettext('Description'); }

        attrs.showPlaceholder = attrs.showPlaceholder == 'true';
        if (!attrs.placeholderText || !attrs.showPlaceholder) { attrs.placeholderText = ''; }

        attrs.showLabel = attrs.showLabel == 'true';
        if(attrs.showLabel === false) {
          attrs.inputClass = '';
        }

        if (!attrs.n) { attrs.n = getRandomInt(1000000,9999999); }

        attrs.required = attrs.required == 'true';

        if (!attrs.messageRequired) { attrs.messageRequired = gettext('This field is required.'); }
      },
      template: '<div class="{{ divClass }}" ng-class="{ \'has-error\' : !form.description.$valid && !form.description.$pristine }">' +
                  '<label ng-show="showLabel" class="{{ labelClass }} control-label" ng-class="{ \'required\' : required || differs }" for="name_{{ n }}" translate>{{ labelText }}</label>' +
                  '<div class="{{ inputClass }}">' +
                    '<textarea name="description" id="description" placeholder="{{ placeholderText }}" ng-model="model" class="{{ textareaClass }}" />' +
                    '<div class="help-block" ng-messages="form.description.$error" ng-show="!form.description.$pristine">' +
                      '<div ng-message="required"><span>{{ messageRequired }}</span></div>' +
                    '</div>' +
                  '</div>' +
                '</div>'
    };
  });
