'use strict';

/**
 * @ngdoc directive
 * @name simpleRepairApp.directive:submitButton
 * @description
 */
angular.module('simpleRepairApp')
  .directive('submitButton', function (gettext, flashMessage) {

    var iconDuration = 2000;

    return {
      restrict: 'E',
      require: ['form'],
      scope: {
        'form': '=form',
        'disabled': '=',
        'buttonText': '@',
        'buttonClass': '@',
        'divClass': '@',
        'tracker': '=',
        'disabledOnPristine': '@',
        'allowIcon': '@',
        'allowFormErrorIcon': '@',
        'formErrorMessage': '@'
      },
      compile: function(element, attrs)
      {
        if (!attrs.buttonText) { attrs.buttonText = gettext('Save'); }
        if (!attrs.buttonClass) { attrs.buttonClass = 'btn-repair-primary btn-raised'; }
        if (!attrs.divClass) { attrs.divClass = 'form-group text-right'; }
        if (!attrs.formErrorMessage) { attrs.formErrorMessage = gettext('Form data is not valid.'); }

        attrs.disabledOnPristine === 'false' ? attrs.disabledOnPristine = false : attrs.disabledOnPristine = true;
        attrs.allowIcon === 'false' ? attrs.allowIcon = false : attrs.allowIcon = true;

        attrs.allowFormErrorIcon = attrs.allowFormErrorIcon == 'true';

      },
      controller: function($scope, $timeout)
      {
        $scope.showingIcon = false;
        $scope.icon = '';

        $scope.$watch('tracker.active()', function(newValue, oldValue) {

          if(newValue === false && oldValue === true) // Just when loading is finished
          {
            $scope.icon = flashMessage.getIcon();
            $scope.showingIcon = true;

            $timeout(function() {

              if($scope.icon === 'ok')
              {
                $scope.form.$setPristine();
              }
              $scope.showingIcon = false;
              $scope.icon = '';
            }, iconDuration);
          }
        });

      },
      template: '<div class="{{ divClass }}">' +
                  '<button type="submit" class="btn {{ buttonClass }}" ng-disabled="(disabledOnPristine && form.$pristine) || !form.$valid || disabled || tracker.active() || showingIcon" ng-class="tracker.active() ? \'tracking\' : \'not-tracking\'">' +
                    '<span class="glyphicon glyphicon-refresh fa-spin" ng-show="tracker.active()"></span>' +
                    '<span class="glyphicon glyphicon-{{ icon }}" ng-show="allowIcon && showingIcon"></span>' +
                    '<i class="fa fa-exclamation-circle error" ng-show="allowFormErrorIcon && !form.$pristine && !form.$valid" title="{{ formErrorMessage }}"></i> {{ buttonText }}' +
                  '</button>' +
                '</div>',
      replace: true
    };
  });
