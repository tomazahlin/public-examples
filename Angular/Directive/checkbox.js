'use strict';

/**
 * @ngdoc directive
 * @name simpleRepairApp.directive:checkboxField
 * @description Only one of these allowed per form. If needed, make nested forms with ng-form.
 */
angular.module('simpleRepairApp')
  .directive('checkboxField', function (gettext) {
    return {
      restrict: 'E',
      require: ['form', 'ngModel'],
      replace: true, // Template must have exactly one root element
      scope: {
        'form': '=form',
        'model': '=ngModel',
        'labelClass': '@',
        'inputClass': '@',
        'divClass': '@',
        'labelText': '@',
        'showLabel': '@',
        'required': '@',
        'messageRequired': '@',
        'n': '@'
      },
      compile: function(element, attrs)
      {
        if (!attrs.labelClass) { attrs.labelClass = 'col-sm-4'; }
        if (!attrs.inputClass) { attrs.inputClass = 'col-sm-8'; }
        if (!attrs.divClass) { attrs.divClass = 'form-group'; }

        if (!attrs.labelText) { attrs.labelText = ''; }

        attrs.showLabel = attrs.showLabel == 'true';
        if(attrs.showLabel === false) {
          attrs.inputClass = '';
        }

        if (!attrs.n) { attrs.n = getRandomInt(1000000,9999999); }

        attrs.required = attrs.required == 'true';

        if (!attrs.messageRequired) { attrs.messageRequired = gettext('This checkbox is required.'); }
      },
      template: '<div class="{{ divClass }}" ng-class="{ \'has-error\' : !form.check.$valid && !form.check.$pristine }">' +
                  '<label ng-show="showLabel" class="{{ labelClass }} control-label" ng-class="{ \'required\' : required || differs }" for="check_{{ n }}">{{ labelText }}</label>' +
                  '<div class="{{ inputClass }}">' +
                    '<ng-checkbox ng-model="model" ng-size="small"></ng-checkbox>' +
                    '<input class="hidden" name="check" type="checkbox" id="check_{{ n }}" ng-model="model" value="1" ng-required="required" />' +
                    '<div class="help-block" ng-messages="form.check.$error" ng-show="!form.check.$pristine">' +
                      '<div ng-message="required"><span>{{ messageRequired }}</span></div>' +
                    '</div>' +
                  '</div>' +
                '</div>'
    };
  });
