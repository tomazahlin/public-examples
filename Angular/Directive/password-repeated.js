'use strict';

/**
 * @ngdoc directive
 * @name simpleRepairApp.directive:passwordRepeated
 * @description
 */
angular.module('simpleRepairApp')
  .directive('passwordRepeated', function () {
    return {
      restrict: 'E',
      require: ['form', 'ngModel', 'ngModelRepeat'],
      scope: {
        'form': '=form',
        'model': '=ngModel',
        'modelRepeat': '=ngModelRepeat',
        'labelClass': '@',
        'inputClass': '@',
        'divClass': '@'
      },
      compile: function(element, attrs)
      {
        if (!attrs.labelClass) { attrs.labelClass = 'col-sm-4'; }
        if (!attrs.inputClass) { attrs.inputClass = 'col-sm-8'; }
        if (!attrs.divClass) { attrs.divClass = 'form-group'; }
      },
      controller: function($scope) {
        $scope.$watch('model', function() {
          if($scope.model !== undefined) {
            $scope.modelRepeat = '';
          }
        });
      },
      template: '<div class="{{ divClass }}" ng-class="{ \'has-error\' : !form.password.$valid && !form.password.$pristine }">' +
                  '<label class="{{ labelClass }} control-label required" for="password" translate>Password</label>' +
                  '<div class="{{ inputClass }}">' +
                    '<input name="password" class="form-control" type="password" id="password" ng-model="model" password-validator required autofill>' +
                    '<div class="help-block" ng-messages="form.password.$error" ng-show="!form.password.$valid && !form.password.$pristine">' +
                      '<div ng-message="short"><span translate>Your password is too short.</span></div>' + // Important, this message must be before required to display properly
                      '<div ng-message="required"><span translate>Please enter your password.</span></div>' +
                      '<div ng-message="long"><span translate>Your password is too long.</span></div>' +
                    '</div>' +
                  '</div>' +
                '</div>' +

                '<div class="{{ divClass }}" ng-class="{ \'has-error\' : !form.passwordRepeat.$valid && !form.passwordRepeat.$pristine }">' +
                  '<label class="{{ labelClass }} control-label required" for="passwordRepeat" translate>Password (repeat)</label>' +
                  '<div class="{{ inputClass }}">' +
                    '<input name="passwordRepeat" class="form-control" type="password" id="passwordRepeat" ng-model="modelRepeat" ng-model-match="model" required autofill match-validator>' +
                    '<div class="help-block" ng-messages="form.passwordRepeat.$error" ng-show="!form.passwordRepeat.$valid && !form.passwordRepeat.$pristine">' +
                      '<div ng-message="required"><span translate>Please repeat your password.</span></div>' +
                      '<div ng-message="match"><span translate>Passwords do not match.</span></div>' +
                    '</div>' +
                  '</div>' +
                '</div>'
    };
  });
