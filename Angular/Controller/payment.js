'use strict';

/**
 * @ngdoc function
 * @name simpleRepairApp.controller:PaymentCtrl
 * @description
 */
angular.module('simpleRepairApp')
  .controller('PaymentCtrl', function ($scope, Package, promiseTracker) {

    $scope.loadingTracker = promiseTracker();

    $scope.packages = {
      subscription: [],
      location: {
        suggested: [],
        other: []
      }
    };

    var subscriptionPackages = Package.subscription().then(
      function(response) {
        $scope.packages.subscription = response;
      }
    );
    $scope.loadingTracker.addPromise(subscriptionPackages);

    var locationPackages = Package.location().then(
      function(response) {
        $scope.packages.location.other = response;
      }
    );
    $scope.loadingTracker.addPromise(locationPackages);

    var suggestedLocationPackages = Package.locationSuggested().then(
      function(response) {
        $scope.packages.location.suggested = response;
      }
    );
    $scope.loadingTracker.addPromise(suggestedLocationPackages);

  });
