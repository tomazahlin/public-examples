'use strict';

/**
 * @ngdoc function
 * @name simpleRepairApp.controller:SupplyCtrl
 * @description
 */
angular.module('simpleRepairApp')
  .controller('SupplyCtrl', function($scope, $state, $stateParams, Supply, Storage, Restangular, flashMessage, gettext, promiseTracker) {

    $scope.loadingTracker = promiseTracker();
    $scope.savingTracker = promiseTracker();

    $scope.form.supply = {
      data: {}
    };

    if($stateParams.id !== undefined)
    {
      var supply = Supply.get($stateParams.id).then(
        function success(supply) {
          $scope.form.supply.data = supply;
          $scope.form.supply.type = true; // Existing

          $scope.form.supply.data.timeReceived = new Date(Date.parse($scope.form.supply.data.timeReceived));
          var supplier = supply.supplier;
          Restangular.restangularizeElement('', supplier, 'suppliers');

          $scope.form.supply.data.supplierId = {
            text: supplier.getAutocompleteText(),
            id: supplier.id,
            data: supplier
          };

          var supplyInventories = Supply.inventories($stateParams.id).then(
            function(response) {
              $scope.form.supply.data.inventories = response;

              for(var i=0; i<$scope.form.supply.data.inventories.length; i++)
              {
                var service = Restangular.restangularizeElement('', $scope.form.supply.data.inventories[i].service, 'services');

                $scope.form.supply.data.inventories[i].serviceAutocomplete = {
                  text: service.getAutocompleteText(),
                  id: service.id,
                  data: service
                };
                $scope.form.supply.data.inventories[i].serviceId = $scope.form.supply.data.inventories[i].service.id;
              }

            }
          );
          $scope.loadingTracker.addPromise(supplyInventories);

        }
      );
      $scope.loadingTracker.addPromise(supply);
    }
    else
    {
      $scope.form.supply.data = Supply.create();
      $scope.form.supply.type = false; // Unexisting
    }

    $scope.save = function() {
      var existing = $stateParams.id !== undefined;
      var promise = Supply.save($scope.form.supply.data).then(
        function(response) {
          if(!existing){
            flashMessage.set('success', gettext('Supply was successfully saved.'), false);
            $state.go('supply-edit', {id:response.id}); // Change state when new supply is saved
          }
          else {
            flashMessage.setIcon('success');
          }
        },
        function() {
          flashMessage.set('error', gettext('There was an error saving the supply.'));
        });
      $scope.savingTracker.addPromise(promise);
    };

  });
