'use strict';

/**
 * @ngdoc function
 * @name simpleRepairApp.controller:LoginCtrl
 * @description
 */
angular.module('simpleRepairApp')
  .controller('LoginCtrl', function($scope, $state, User, flashMessage, auth, gettext, promiseTracker) {

    $scope.loginTracker = promiseTracker();

    $scope.user = {};

    $scope.login = function() {

      var login = User.login($scope.user).then(
        function(response) {
          auth.set(response);
          $state.go('home');
        },
        function() {
          flashMessage.set('error', gettext('Bad credentials.'));
        }
      );

      $scope.loginTracker.addPromise(login);

    };

  });
