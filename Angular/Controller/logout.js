'use strict';

/**
 * @ngdoc function
 * @name simpleRepairApp.controller:LogoutCtrl
 * @description
 */
angular.module('simpleRepairApp')
  .controller('LogoutCtrl', function($scope, User, auth, flashMessage, gettext) {

    $scope.logout = function() {
      User.logout().then(
        function() {
          flashMessage.set('success', gettext('You have successfully logged out.'), false);
          auth.logout();
        }
      );

    };

  });
