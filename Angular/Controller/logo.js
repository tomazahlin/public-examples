'use strict';

/**
 * @ngdoc function
 * @name simpleRepairApp.controller:AccountLogoCtrl
 * @description
 */
angular.module('simpleRepairApp')
  .controller('AccountLogoCtrl', function($scope, $timeout, Storage, Restangular, Account, flashMessage, gettext) {

    var setDefaultData = function() {

      $scope.form.logo = {
        account: {}
      };

    };

    setDefaultData();

    Storage.getResource('account').then(function success(data) {
      $timeout(function(){ // To force the view to see the updated scope
        $scope.form.logo.account = data;
      });
    });

    $scope.onFileSelect = function($files) {
      //$files: an array of files selected, each file has name, size, and type.
      var file = $files[0];

      Account.uploadLogo(file).then(
        function(response) {
          $scope.form.logo.account.setLogo(response.logo);
          emitLogo();
          flashMessage.set('success', gettext('Account logo was successfully uploaded.'));
        },
        function() {
          flashMessage.set('error', gettext('There was an error uploading the logo.'));
        }
      );
    };

    $scope.removeLogo = function() {

      Account.removeLogo().then(
        function() {
          $scope.form.logo.account.removeLogo();
          emitLogo();
          flashMessage.set('success', gettext('Account logo was successfully removed.'));
        },
        function() {
          flashMessage.set('error', gettext('There was an error while removing the logo.'));
        }
      );

    };

    var emitLogo = function() {
      $scope.$emit('logoChanged', { logo: $scope.form.logo.account.logo });
    };

  });