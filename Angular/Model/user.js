'use strict';

/**
 * @ngdoc service
 * @name simpleRepairApp.User
 * @description
 */
angular.module('simpleRepairApp')
  .factory('User', function(roles, Restangular, CHECKER_URL, PUBLIC_URL) {

    Restangular.extendModel('users', function(user) {

      user.has = function(role) {
        return roles.has(user.group.roles, role);
      };

      user.isStripeCustomer = function()
      {
        return user.stripeId !== undefined && user.stripeId.length > 0;
      };

      user.getCredentials = function()
      {
        if(user.firstName !== undefined && user.lastName !== undefined)
        {
          return user.firstName.substring(0,1) + user.lastName.substring(0,1);
        }
        else
        {
          return undefined;
        }
      };

      user.hasCredentials = function()
      {
        return user.getCredentials() !== undefined;
      };

      return user;

    });

    return {
      login: function(data) {
        return Restangular.allUrl('login', CHECKER_URL + '/login').post(data);
      },
      logout: function() {
        return Restangular.all('logout').remove();
      },
      get: function(id) {
        return Restangular.one('users', id).get();
      },
      update: function(data) {
        return data.post();
      },
      all: function(query) {
        if(query === undefined) query = {};
        return Restangular.all('users').getList('', query);
      },
      invite: function(data) {
        return Restangular.all('invite').post(data);
      },
      requestPasswordReset: function(data) {
        return Restangular.allUrl('reset', PUBLIC_URL + '/reset').post(data);
      },
      checkPasswordResetToken: function(token) {
        return Restangular.allUrl('users', PUBLIC_URL + '/reset').get(token);
      },
      resetPassword: function(token, data) {
        return Restangular.allUrl('reset', PUBLIC_URL + '/reset/' + token).post(data);
      },
      cancelPasswordReset: function(token) {
        return Restangular.allUrl('reset', PUBLIC_URL + '/reset/' + token + '/cancel').post();
      },
      restangularize: function(user) {
        if(user !== undefined) {
          return Restangular.restangularizeElement('', user, 'users');
        }
        else {
          return undefined;
        }
      }
    };

  });
