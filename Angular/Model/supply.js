'use strict';

/**
 * @ngdoc service
 * @name simpleRepairApp.Supply
 * @description
 */
angular.module('simpleRepairApp')
  .factory('Supply', function(Restangular, Inventory) {

    Restangular.extendModel('supplies', function(supply) {

      supply.totalWithoutTax = function() {
        var total = 0.0;
        if(supply.inventories)
        {
          for(var i=0, l=supply.inventories.length; i<l; i++)
          {
            total += supply.inventories[i].totalWithoutTax();
          }
        }
        return total;
      };

      supply.init = function() {
        supply.timeReceived = today();
      };

      supply.totalWithTax = function() {
        var total = 0.0;
        if(supply.inventories)
        {
          for(var i=0, l=supply.inventories.length; i<l; i++)
          {
            total += supply.inventories[i].totalWithTax();
          }
        }
        return total;
      };

      supply.totalWithShippingAndOtherCosts = function() {
        var shipping = 0;
        var other = 0;

        if(supply.shippingCost !== undefined)
        {
          shipping = supply.shippingCost;
        }

        if(supply.otherCost !== undefined)
        {
          other = supply.otherCost;
        }

        return supply.totalWithTax() + shipping + other;
      };

      supply.addNewInventory = function()
      {
        var service = Inventory.create();
        supply.inventories.push(service);
      };

      supply.removeInventory = function(index)
      {
        //var index = $scope.supply.inventories.indexOf(service);
        if (index > -1) {
          supply.inventories.splice(index, 1);
        }
      };

      supply.hasManyInventories = function() {
        return supply.inventories.length > 1;
      };

      // One is always present at the start
      supply.inventories = [];
      supply.addNewInventory();

      return supply;
    });
    
    return {
      create: function() {
        return Restangular.restangularizeElement('', {}, 'supplies');
      },
      get: function(id) {
        return Restangular.one('supplies', id).get();
      },
      find: function(term) {
        return Restangular.all('supplies').all('find').getList(term);
      },
      inventories: function(id) {
        return Restangular.one('supplies', id).getList('inventories');
      },
      save: function(d) {
        var data = Restangular.copy(d);

        var date = data.timeReceived;     // YYYY                  0-11                    1-31
        data.timeReceived = {year: date.getFullYear(), month: date.getMonth()+1, day: date.getDate()};

        data.inventories = [];

        for(var i = 0; i<d.inventories.length; i++)
        {
          data.inventories.push({
            serviceId: d.inventories[i].serviceAutocomplete.id,
            quantity: d.inventories[i].quantity,
            value: d.inventories[i].value,
            tax: d.inventories[i].tax
          });
        }

        if(data.supplierId.id !== undefined) {
          data.supplierId = data.supplierId.id;
        }
        return data.post();
      }
    };

  });
