'use strict';

/**
 * @ngdoc service
 * @name simpleRepairApp.Item
 * @description
 */
angular.module('simpleRepairApp')
  .factory('Item', function(Restangular) {

    return {
      get: function(id) {
        return Restangular.one('items', id).get();
      },
      getCustomerItems: function(id) {
        return Restangular.one('customers', id).getList('items');
      }
    };

  });
