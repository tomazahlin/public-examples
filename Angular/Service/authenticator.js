'use strict';

/**
 * @ngdoc service
 * @name simpleRepairApp.authenticatorInterceptor
 * @description
 */
angular.module('simpleRepairApp')
  .factory('authenticatorInterceptor', function($location, $q, localStorageService, flashMessage, gettext) {
    return {
      'responseError': function(response) {

        var fatal = false;

        if (response.status === 401) {
          // handle the case where the user is not authenticated
          fatal = true;
          flashMessage.set('error', gettext('Authentication failure.'), false);

        }
        else if (response.status === 500) {
          // handle the case where the internal server error occurs
          fatal = true;
          flashMessage.set('error', gettext('Server failure.'), false);
        }

        if(fatal)
        {
          localStorageService.clearAll(); // Auth will detect and clean the rest of the sensitive data
          $location.path('/');
        }

        return $q.reject(response);
      }
    };
  });
