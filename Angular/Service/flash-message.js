'use strict';

/**
 * @ngdoc factory
 * @name simpleRepairApp.flashMessage
 * @description
 */
angular.module('simpleRepairApp')
  .factory('flashMessage', function($timeout, localStorageService, flash) {

    var delay = 50; // ms

    var icon; // For submit buttons

    var hasMessage = function() {
      return localStorageService.get('_flashMessage') !== null;
    };

    var getMessage = function() {
      var msg = localStorageService.get('_flashMessage');
      localStorageService.remove('_flashMessage');
      return msg;
    };

    var getIconByType = function(type)
    {
      if(type === 'success' || type === 'ok')
      {
        return 'ok';
      }
      return 'remove';
    };

    return {

      /**
       * Use this method to set flash messages because it supports messages for next state, not only for the current state.
       * The message passed in must already be translated in controller
       * @param type
       * @param msg
       * @param now
       */
      set: function(type, msg, now)
      {
        if(now === undefined) now = true;

        if(now) {
          flash[type] = msg;
        }
        else {
          localStorageService.set('_flashMessage', {type: type, text: msg});
        }
        icon = getIconByType(type);

      },
      run: function() {
        if(hasMessage())
        {
          var msg = getMessage();
          $timeout( function () {
            flash[msg.type] = msg.text;
          }, delay);
        }
      },
      setIcon: function(type) {
        icon = getIconByType(type);
      },
      getIcon: function() {
        return icon;
      }
    };
  });
