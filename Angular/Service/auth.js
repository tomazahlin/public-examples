'use strict';

/**
 * @ngdoc factory
 * @name simpleRepairApp.auth
 * @description
 */
angular.module('simpleRepairApp')
  .factory('auth', function($rootScope, $http, $location, $state, roles, localStorageService) {

    var isPublic = function(state) {
      if(state === undefined) {
        state = $state.current;
      }
      return state.publicUrl;
    };

    var hasRequiredRole = function(state) {
      if(state === undefined) {
        state = $state.current;
      }
      return state.requiredRole !== undefined;
    };

    var getRequiredRole = function(state) {
      if(state === undefined) {
        state = $state.current;
      }
      if(state.requiredRole !== undefined)
      {
        return state.requiredRole;
      }
      return null;
    };

    var isPrivate = function(state) {
      return !isPublic(state);
    };

    var getRoles = function() {
      return localStorageService.get('userRoles');
    };

    var clearStorage = function() // Preserves flash messages
    {
      if(localStorageService.get('_flashMessage') !== null)
      {
        var message = localStorageService.get('_flashMessage');
      }

      localStorageService.clearAll();

      if(message !== undefined)
      {
        localStorageService.set('_flashMessage', message);
      }

    };

    var clearHttp = function()
    {
      delete $http.defaults.headers.common['X-Auth-Token'];
      delete $http.defaults.headers.common['Location'];
      // console.log('deleted token');
    };

    return {

      logout: function() { // Used on logout
        clearStorage();
        clearHttp();
        $state.go('login');
      },

      isTokenSet: function() {
        return ($http.defaults.headers.common['X-Auth-Token'] !== undefined && $http.defaults.headers.common['X-Auth-Token'] !== null);
      },
      isPublic: function() {
        return isPublic();
      },
      isPrivate: function() {
        return isPrivate();
      },

      getToken: function() {
        return localStorageService.get('token');
      },
      getAccountId: function() {
        return parseInt(localStorageService.get('accountId'));
      },
      getUserId: function() {
        return parseInt(localStorageService.get('userId'));
      },

      getRoles: function() {
        return getRoles();
      },
      hasRole: function(role) {
        return roles.has(getRoles(), role);
      },

      allowedSref: function(sref) {
        return roles.has(getRoles(), getRequiredRole($state.get(sref)));
      },

      set: function(data) {
        // Data set upon login
        localStorageService.set('userId',    data.userId);
        localStorageService.set('accountId', data.accountId);
        localStorageService.set('token',     data.token);
        localStorageService.set('userRoles', data.roles);
        $http.defaults.headers.common['X-Auth-Token'] = data.token;
      },

      hasDefaultLocation: function() {
        return localStorageService.get('location') !== null;
      },
      getDefaultLocation: function() {
        return parseInt(localStorageService.get('location'));
      },
      setDefaultLocation: function(id) {
        localStorageService.set('location', id);
        $http.defaults.headers.common['Location'] = id;
      },

      run: function(state) {

        // console.log('Auth run');

        var authenticated = true; // assuming it is true

        // Location is optional, upon login it is unset (primary location used)
        if (localStorageService.get('token') === null ||
            localStorageService.get('userId') === null ||
            localStorageService.get('accountId') === null ||
            $http.defaults.headers.common['X-Auth-Token'] === undefined) {
          // console.log('something not set');

          clearStorage();
          clearHttp();

          authenticated = false;
        }
        else
        {
          // console.log($http.defaults.headers.common['X-Auth-Token']);
          // console.log('all set');
        }

        // Clear scope if needed

        if(isPublic(state) && !authenticated) {
          // console.log('Auth not - clearing scope');
          $rootScope.$broadcast('clearScope');
        }

        // Actions

        if(isPrivate(state) && !authenticated) {
          // console.log('Auth fail - case 1');
          $state.go('login');
          return false;
        }
        else if(isPublic(state) && authenticated) {
          // console.log('Auth set - case 2');
          $state.go('home');
          return false;
        }
        else if(hasRequiredRole(state) && !roles.has(getRoles(), getRequiredRole(state))) {
          // console.log('Auth fail - roles');
          $state.go('home');
          return false;
        }
        else {
          // console.log('Auth ok');
          return true;
        }
      }
    };
  });
