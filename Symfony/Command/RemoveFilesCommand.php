<?php
/**
 * Created by PhpStorm.
 * User: tomaz
 * Date: 04/04/14
 * Time: 14:43
 */

namespace Service\Bundle\BaseBundle\Command;

use Service\Bundle\BaseBundle\Helper\Symfony;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

class RemoveFilesCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('service:files:remove')
             ->setDescription('Use this command to remove public files (repair bar code images, repair PDF documents, sale PDF documents...) under public web directory.')
             ->addOption('force', null, InputOption::VALUE_NONE, 'Set this parameter to execute this command');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if($input->getOption('force'))
        {
            $dir = substr(Symfony::getFilesFolder(),0,-1); # Removes the last slash

            $fs = new Filesystem();

            $fs->remove($dir);

            if(!$fs->exists($dir))
            {
                $output->writeln('<fg=green>Success:</fg=green> All files deleted.');
            }
            else
            {
                $output->writeln('<error>Error:</error> Could not remove all files.');
            }

            $fs->mkdir($dir);

            if($fs->exists($dir))
            {
                $output->writeln('<fg=green>Success:</fg=green> New empty folder for files was created.');
            }
            else
            {
                $output->writeln('<error>Error:</error> Could not create new empty folder for files.');
            }

            return 1;

        }
        else {

            $output->writeln('<error>ATTENTION:</error> This command should not be executed on live server.');
            $output->writeln('Please run the operation with --force to execute');
            $output->writeln('<error>Data will be lost!</error>');

            return 0;

        }
    }
} 