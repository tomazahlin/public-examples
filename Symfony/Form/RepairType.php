<?php
/**
 * Created by PhpStorm.
 * User: tomaz
 * Date: 31/03/14
 * Time: 10:06
 */

namespace Service\Bundle\RepairBundle\Form\Type;

use Monolog\Logger;
use Service\Bundle\BaseBundle\Helper\Service;
use Service\Bundle\CustomerBundle\Entity\Customer;
use Service\Bundle\CustomerBundle\DataTransformer\AutocompleteToCustomerTransformer;
use Service\Bundle\CustomerBundle\Form\Type\CustomerArchiveType;
use Service\Bundle\RepairBundle\DataTransformer\AutocompleteToItemTransformer;
use Service\Bundle\RepairBundle\Entity\Item;
use Service\Bundle\RepairBundle\Entity\Repair;
use Service\Bundle\UserBundle\Entity\User;
use Service\Bundle\UserBundle\Security\AccountSecurity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Exception\LogicException;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class RepairType extends AbstractType
{
    /**
     * @var AutocompleteToCustomerTransformer
     */
    private $customerTransformer;

    /**
     * @var AutocompleteToItemTransformer
     */
    private $itemTransformer;

    /**
     * @var AccountSecurity
     */
    private $security;

    public function __construct(AutocompleteToCustomerTransformer $customerTransformer,
                                AutocompleteToItemTransformer $itemTransformer,
                                AccountSecurity $accountSecurity)
    {
        $this->customerTransformer = $customerTransformer;
        $this->itemTransformer = $itemTransformer;
        $this->security = $accountSecurity;
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $options['type'] == 'new' ? $view->vars['new'] = true : $view->vars['new'] = false;
        $view->vars['update'] = !$view->vars['new'];
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('itemArchive',    new ItemArchiveType())
                ->add('comment',        'textarea',         array('required' => false))
                ->add('priority',       'choice',           array('choices' => Service::getCustomerStatusPriorities(1)))
                ->add('due',            'datetime',         array('required' => false))
                ->add('sendEmail',      'checkbox',         array('required' => false))
                ->add('sendSms',        'checkbox',         array('required' => false))
                ->add('call',           'checkbox',         array('required' => false))
                ->add('customerDiscount','hidden',          array('mapped' => false, 'required' => false, 'disabled' => true)) # Used for auto-completing services
                ->add('customerArchive', new CustomerArchiveType());

        if($this->validateTypeOption($options['type']) == 'new')
        {
            $builder->add('customerAutocomplete',  'text',       array('mapped' => false, 'required' => false))
                    ->add('customerId',            'hidden',     array('mapped' => false, 'label' => false))
                    ->add('itemId',                'hidden',     array('mapped' => false, 'label' => false));

            $builder->addEventListener(FormEvents::SUBMIT, function (FormEvent $event)
            {
                $repair = $event->getData();
                $this->validateRepairInstance($repair);

                # Customer
                $customerId = $event->getForm()->get('customerId')->getData();
                $customer = $this->customerTransformer->transform($customerId);
                if(!($customer instanceof Customer && $this->getUser()->getAccount() === $customer->getAccount()))
                {
                    $customer = new Customer();
                    $customer->setStatus($repair->getCustomerArchive()->getStatus());
                }
                $customer->import($repair->getCustomerArchive()->export()); # Update customer with latest data, or to insert new customer
                $repair->setCustomer($customer);

                # Item
                $itemId = $event->getForm()->get('itemId')->getData();
                $item = $this->itemTransformer->transform($itemId);
                if(!($item instanceof Item && $item->getCustomer() === $repair->getCustomer()))
                {
                    $item = new Item();
                    $item->setCustomer($customer);
                }
                $item->import($repair->getItemArchive()->export()); # Update item with latest data, or to insert new item
                $repair->setItem($item);

                $event->setData($repair);
            });
        }
        else
        {
            $repair = $builder->getData();
            $this->validateRepairInstance($repair);

            $builder->add('paid',           'checkbox',     array('required' => false))
                    ->add('pickedUp',       'checkbox',     array('required' => false));

            $builder->get('customerDiscount')->setData($repair->getCustomer()->getStatus()->getDiscount());
        }

        $builder->add('serviceArchive',   'collection',   array(
                                                              'type' => 'service_archive',
                                                              'options' => array(
                                                                  'data_class' => 'Service\Bundle\RepairBundle\Entity\ServiceRepairArchive'
                                                              ),
                                                              'allow_add' => true,
                                                              'allow_delete' => true,
                                                              'prototype_name' => '__prototype__',
                                                              'by_reference' => false,
                                                              'error_bubbling' => false
                                                          )
        );

        $builder->add('save',       'submit');
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver
            ->setDefaults(array(
                'allow_extra_fields' => true,
                'data_class'        => 'Service\Bundle\RepairBundle\Entity\Repair',
                'error_mapping'     => array(
                                            'callValid' => 'call',
                                            'sendSmsValid' => 'sendSms',
                                            'sendEmailValid' => 'sendEmail'
                                       ),
                'cascade_validation' => true
            ))
            ->setRequired(array('type'));
    }

    /**
     * @return User
     */
    private function getUser()
    {
        return $this->security->getUser();
    }

    public function getName()
    {
        return 'service_repair';
    }

    private function validateTypeOption($type)
    {
        if(!in_array($type,['new', 'update']))
        {
            throw new LogicException('The type option is not valid.');
        }

        return $type;
    }

    private function validateRepairInstance($repair)
    {
        if (!$repair instanceof Repair) {
            throw new LogicException('Data must be an instance of Repair!');
        }
    }
}