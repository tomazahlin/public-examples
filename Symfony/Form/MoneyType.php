<?php
/**
 * Created by PhpStorm.
 * User: tomaz
 * Date: 31/03/14
 * Time: 10:06
 */

namespace Service\Bundle\RepairBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MoneyType extends AbstractType
{
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'allow_extra_fields' => true,
            'currency' => 'USD',
            'precision' => 4
        ));
    }

    public function getParent()
    {
        return 'money';
    }

    public function getName()
    {
        return 'money_value';
    }
}