<?php
/**
 * Created by PhpStorm.
 * User: tomaz
 * Date: 31/03/14
 * Time: 10:06
 */

namespace Service\Bundle\CustomerBundle\Form\Type;

use Service\Bundle\BaseBundle\Helper\Service;
use Service\Bundle\CustomerBundle\Entity\Customer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CustomerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $customer = $builder->getData();

        $countryOptions = array(
            'required' => false,
            'choices' => Service::getCountries(1)
        );

        if(((!$customer instanceof Customer) || $customer->getId() == null) && $options['standalone'] === true)
        {
            $countryOptions['data'] = 'US'; # Default value
        }

        if($options['standalone'] === true)
        {
            $standAloneOptions = array('required' => true);
            $builder->add('statuses',       'status_select',    array('required' => false, 'mapped' => false, 'add_attributes' => true));
        }
        else
        {
            $standAloneOptions = array('required' => false);
        }

        $builder->add('firstName',      'text',             $standAloneOptions)
                ->add('lastName',       'text',             $standAloneOptions)
                ->add('telephone',      'text',             array('required' => false))
                ->add('email',          'email',            array('required' => false))

                ->add('address',        'text',             array('required' => false))
                ->add('addressSecond',  'text',             array('required' => false))
                ->add('city',           'text',             array('required' => false))
                ->add('zipCode',        'text',             array('required' => false))
                ->add('state',          'text',             array('required' => false))
                ->add('country',        'choice',           $countryOptions)

                ->add('companyName',    'text',             array('required' => false))
                ->add('taxNumber',      'text',             array('required' => false))
                ->add('taxPayer',       'checkbox');

        $builder->add('status',         new StatusType(),   array('data_class' => 'Service\Bundle\CustomerBundle\Entity\CustomerStatus',
                                                                  'standalone' => $options['standalone'],
                                                                  'validation_groups' => $options['validation_groups']))
                ->add('save',           'submit');

        $builder->get('status')->remove('save');

    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'allow_extra_fields' => true,
            'data_class' => 'Service\Bundle\CustomerBundle\Entity\Customer',
            'standalone' => true, # If not included in repair/sale form,
            'validation_groups' => array('mandatory'),
            'cascade_validation' => true
        ));
    }

    public function getName()
    {
        return 'service_customer';
    }
}