<?php
/**
 * Created by PhpStorm.
 * User: tomaz
 * Date: 06/05/14
 * Time: 15:33
 */

namespace Service\Bundle\RepairBundle\Entity\Listener;

use Service\Bundle\RepairBundle\Entity\Barcode;
use Service\Bundle\RepairBundle\Entity\Repair;

use Service\Bundle\UserBundle\Entity\User;

class RepairListener extends InvoiceListener
{
    public function prePersistHandler(Repair $repair)
    {
        $location = $this->container->get('service.security')->getDefaultLocation();

        if($this->getUser() instanceof User) # Only during application, not during fixtures
        {
            $repair
                ->setUser($this->getUser())
                ->setLocation($location)
                ->setTrashed(false)
                ->addFollower($this->getUser());
        }

        $repair->generateCode()
               ->generateTrackingNumber();
    }

    public function postPersistHandler(Repair $repair)
    {
        $code = $repair->getCode();
        $barcode = new Barcode();
        $barcode->setMode(Barcode::MODE_PNG);

        $this->container->get('filesystem')->mkdir(dirname($repair->getBarcodeFilename()), 0777, true);

        $barcode->saveImageToFile($code, $repair->getBarcodeFilename());

        $this->removeStock($repair);
    }

    public function postUpdateHandler(Repair $repair)
    {
        $this->increaseStock($repair);
        $this->removeStock($repair);
    }

    public function postRemoveHandler(Repair $repair)
    {
        $this->increaseStock($repair);
    }
}