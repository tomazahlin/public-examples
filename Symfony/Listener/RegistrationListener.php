<?php
/**
 * Created by PhpStorm.
 * User: tomaz
 * Date: 13/03/14
 * Time: 13:48
 */

namespace Service\Bundle\UserBundle\Event\Listener;

use Service\Bundle\BaseBundle\Event\Listener\AbstractListener;
use Service\Bundle\UserBundle\Event\InvitationActivationEvent;
use Service\Bundle\UserBundle\Event\RegistrationEvent;
use Service\Bundle\UserBundle\Event\RegistrationPostActivationEvent;
use Service\Bundle\UserBundle\Event\RegistrationPreActivationEvent;

class RegistrationListener extends AbstractListener
{
    public function sendActivationEmailAction(RegistrationEvent $event)
    {
        $account = $event->getAccount();
        $user    = $event->getUser();

        $url = $this->container->get('service.angular_router')->generateUrl('activate/' . $user->getActivationToken());

        $rendered_message = $this->templating->render('ServiceUserBundle:Email:registration_message.html.twig', array(
            'company' => $account->getName(),
            'confirmationUrl' =>  $url
        ));

        $this->mailer->setTemplate('ServiceUserBundle:Email:registration.html.twig');
        $this->mailer->setReceiver($user->getEmail());
        $this->mailer->sendMail($this->trans('Registration'), $this->trans('Thank you for registration!'), $rendered_message, []);
    }

    public function onPreActivationAction(RegistrationPreActivationEvent $event)
    {
        # Adds a first free one-month basic subscription package
        $account = $event->getAccount();
        $packageDuration = $event->getPackageDuration();
        $account->addPackage($packageDuration, 1, true);

        $account->setDateExpireByLastAddedPackage();
    }

    public function onPostActivationAction(RegistrationPostActivationEvent $event)
    {
        $account = $event->getAccount();
        $user    = $account->getUsers()->first(); # Upon activation there is only 1 user

        // Set initial account data here
        $baseStatuses = $this->container->get('service.transformer.base_statuses')->getTransformedStatuses($account);
        $account->setStatuses($baseStatuses);
        // Set initial account data above
        $this->container->get('service.entity_manager')->process('persist', $account);

        $rendered_message = $this->templating->render('ServiceUserBundle:Email:registration_activation_message.html.twig', array(
            'company' => $account->getName(),
            'name' => $user->getFullName()
        ));

        $this->mailer->setTemplate('ServiceUserBundle:Email:registration.html.twig');
        $this->mailer->setReceiver($user->getEmail());
        $this->mailer->sendMail($this->trans('Activation'), $this->trans('Thank you for activating your account! You have received free trial subscription package!'), $rendered_message, []);
    }

    public function onInvitationAction(RegistrationEvent $event)
    {
        $account = $event->getAccount();
        $user    = $event->getUser();

        $url = $this->container->get('service.angular_router')->generateUrl('activate/' . $user->getActivationToken());

        $rendered_message = $this->templating->render('ServiceUserBundle:Email:invitation_message.html.twig', array(
            'company' => $account->getName(),
            'group' => $user->getGroup()->getName(),
            'confirmationUrl' =>  $url
        ));

        $this->mailer->setTemplate('ServiceUserBundle:Email:registration.html.twig');
        $this->mailer->setReceiver($user->getEmail());
        $this->mailer->sendMail($this->trans('Invitation'), $this->trans('You have received an invitation!'), $rendered_message, []);
    }

    public function onInvitationActivationAction(InvitationActivationEvent $event)
    {
        $user = $event->getUser();

        $rendered_message = $this->templating->render('ServiceUserBundle:Email:invitation_activation_message.html.twig', array(
            'name' => $user->getFullName()
        ));

        $this->mailer->setTemplate('ServiceUserBundle:Email:registration.html.twig');
        $this->mailer->setReceiver($user->getEmail());
        $this->mailer->sendMail($this->trans('Activation'), $this->trans('Thank you for activating your account!'), $rendered_message, []);
    }

}