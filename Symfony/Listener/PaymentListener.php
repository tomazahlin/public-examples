<?php
/**
 * Created by PhpStorm.
 * User: tomaz
 * Date: 13/03/14
 * Time: 13:48
 */

namespace Service\Bundle\PaymentBundle\Event\Listener;

use Service\Bundle\BaseBundle\Entity\Attachment;
use Service\Bundle\BaseBundle\Event\Listener\AbstractListener;
use Service\Bundle\PaymentBundle\Event\PaymentEvent;
use Symfony\Component\EventDispatcher\Event;

class PaymentListener extends AbstractListener
{
    /**
     * When user pays for account subscription / location package
     */
    public function onUserPaymentAction(PaymentEvent $event)
    {
        $payment = $event->getPayment();
        $account = $payment->getAccount();
        $user    = $payment->getUser();

        # E-mail for user who bought package

        $invoiceHtml = $this->templating->render('ServicePaymentBundle:Payment:user_payment_invoice.html.twig', array('payment' => $payment));
        $invoiceFile = $this->container->get('knp_snappy.pdf')->getOutputFromHtml($invoiceHtml);
        $invoiceAttachment = new Attachment($invoiceFile, $payment->getInvoiceFilename(), 'application/pdf');

        $renderedMessage = $this->templating->render('ServicePaymentBundle:Email:user_payment_message.html.twig', array(
            'company' => $account->getName(),
            'user' => $user->getFullName(),
            'url' => $this->container->getParameter('service.website.angular')
        ));

        $this->mailer->setTemplate('ServicePaymentBundle:Email:user_payment.html.twig');
        $this->mailer->setReceiver($user->getEmail());
        $this->mailer->sendMail($this->trans('Payment'), $this->trans('Thank you for your payment!'), $renderedMessage, null, null, array($invoiceAttachment));

        # E-mail for staff

        $renderedMessage = $this->templating->render('ServicePaymentBundle:Email:user_payment_notification_message.html.twig', array('payment' => $payment));

        $this->mailer->setTemplate('ServicePaymentBundle:Email:user_payment_notification.html.twig');
        $this->mailer->setReceiver($this->container->getParameter('service.email.info'));
        $this->mailer->sendMail($this->trans('Payment'), $this->trans('Payment received!'), $renderedMessage);
    }

    public function onCustomerPaymentAction()
    {
        // TODO when customer pays for sale / repair
    }

}