<?php
/**
 * Created by PhpStorm.
 * User: tomaz
 * Date: 06/05/14
 * Time: 15:33
 */

namespace Service\Bundle\RepairBundle\Entity\Listener;

use Service\Bundle\BaseBundle\Entity\Listener\ContainerAwareEntityListener;
use Service\Bundle\RepairBundle\Entity\Inventory;
use Service\Bundle\RepairBundle\Entity\Supply;

use Symfony\Component\Security\Core\SecurityContext;

class SupplyListener extends ContainerAwareEntityListener
{
    public function postLoadHandler(Supply $supply)
    {
        $supply->copyInventory();
    }

    public function prePersistHandler(Supply $supply)
    {
        $location = $this->container->get('service.security')->getDefaultLocation();
        $supply->setLocation($location);
        $this->updateNumOfServices($supply);
    }

    public function postPersistHandler(Supply $supply)
    {
        $this->increaseStock($supply);
    }

    public function preUpdateHandler(Supply $supply)
    {
        $supply->setTimeUpdate();
        $this->updateNumOfServices($supply);
    }

    public function postUpdateHandler(Supply $supply)
    {
        $this->removeStock($supply);
        $this->increaseStock($supply);
    }

    public function postRemoveHandler(Supply $supply)
    {
        $this->removeStock($supply);
    }

    /**
     * Adds to stock
     * @param Supply $supply
     */
    private function increaseStock(Supply $supply)
    {
        $location = $this->container->get('service.security')->getDefaultLocation();

        foreach($supply->getInventories() as $inventory)
        {
            if($inventory instanceof Inventory)
            {
                $service = $inventory->getService();
                $service->setStockable(true);                           # Sets service to stockable
                $stock = $service->getStock($location);

                $stock->addToInventorySum($inventory->getQuantity());

                $this->container->get('service.entity_manager')->process('persist', $service, false);
                $this->container->get('service.entity_manager')->process('persist', $stock,   false);
            }
        }
        $this->container->get('service.entity_manager')->flush();
    }

    /**
     * Remove from stock (edit / remove)
     * @param Supply $supply
     */
    private function removeStock(Supply $supply)
    {
        $location = $this->container->get('service.security')->getDefaultLocation();

        foreach($supply->getInventoriesCopy() as $inventory)
        {
            if($inventory instanceof Inventory)
            {
                $service = $inventory->getService();
                $service->setStockable(true);                           # Sets service to stockable
                $stock = $service->getStock($location);

                $stock->removeFromInventorySum($inventory->getQuantity());

                $this->container->get('service.entity_manager')->process('persist', $service, false);
                $this->container->get('service.entity_manager')->process('persist', $stock,   false);
            }
        }
        $this->container->get('service.entity_manager')->flush();
    }

    private function updateNumOfServices(Supply $supply)
    {
        $number = 0;

        foreach($supply->getInventories() as $inventory)
        {
            if($inventory instanceof Inventory)
            {
                $number += $inventory->getQuantity();
            }
        }

        $supply->setNumOfServices($number);
    }
}