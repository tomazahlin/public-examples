<?php
/**
 * Created by PhpStorm.
 * User: tomaz
 * Date: 05/04/14
 * Time: 17:28
 */

namespace Service\Bundle\RepairBundle\DataTransformer;

use Service\Bundle\RepairBundle\Entity\Supplier;
use Service\Bundle\RepairBundle\Repository\SupplierRepository;
use Symfony\Component\Form\DataTransformerInterface;

class AutocompleteToSupplierTransformer implements DataTransformerInterface
{
    /**
     * @var SupplierRepository
     */
    public $supplierRepository;

    /**
     * @param SupplierRepository $supplierRepository
     */
    public function __construct(SupplierRepository $supplierRepository)
    {
        $this->supplierRepository = $supplierRepository;
    }

    /**
     * Transforms Supplier object to id.
     * @param  Supplier|null $supplier
     * @return integer|null
     */
    public function reverseTransform($supplier)
    {
        if($supplier === null)
        {
            return null;
        }

        return $supplier->getId();
    }

    /**
     * Transforms an id to a Supplier object.
     * @param integer $id
     * @return Supplier
     */
    public function transform($id)
    {
        $id = (int)$id;

        if($id === 0)
        {
            return null;
        }
        return $this->supplierRepository->loadOne('id', $id, false);
    }
}