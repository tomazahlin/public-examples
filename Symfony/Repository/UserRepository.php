<?php
/**
 * Created by PhpStorm.
 * User: Tomaz
 * Date: 10.3.2014
 * Time: 10:16
 */

namespace Service\Bundle\UserBundle\Repository;

use Service\Bundle\BaseBundle\Helper\Text;
use Service\Bundle\BaseBundle\Repository\Repository;
use Service\Bundle\UserBundle\Entity\Role;
use Service\Bundle\UserBundle\Entity\User;

use Doctrine\ORM\Query\Expr;

use Doctrine\ORM\NoResultException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UserRepository extends Repository {

    protected $name = 'ServiceUserBundle:User';
    protected $alias = 'u';

    /**
     * @param $property
     * @param $value
     * @param bool $throwException
     * @param null $activated
     * @throws NotFoundHttpException
     * @return User
     */
    public function loadOne($property, $value, $throwException = true, $activated = null)
    {
        $user = parent::loadOne($property, $value, $throwException);

        if($activated === null ||
           $user instanceof User && (
               $activated === true  &&  $user->isActivated() ||
               $activated === false && !$user->isActivated()
           )
        ) {
            return $user;
        }
        throw new NotFoundHttpException('User with requested activation status not found!');
    }

    /**
     * @param $email
     * @return User|null
     */
    public function loadLogin($email)
    {
        $e = Text::canonicalize($email);
        $qb = $this->entityManager->createQueryBuilder();
        $qb->select('u')
            ->from('ServiceUserBundle:User', 'u')
            ->where('u.email = :email')
            ->andWhere('u.activated = :activated')
            ->setMaxResults(1)
            ->setParameter('email', $e)
            ->setParameter('activated', true);
        try {
            return $result = $qb->getQuery()->getSingleResult();
        }
        catch(NoResultException $e) {
            return null;
        }
    }

    /**
     * @param $value
     * @return string|null
     */
    public function getEmailByToken($value)
    {
        $qb = $this->entityManager->createQueryBuilder();
        $qb->select('u.email')
            ->from('ServiceUserBundle:User', 'u')
            ->leftJoin('u.tokens', 't')
            ->where('u.activated = :activated')
            ->andWhere($qb->expr()->eq('t.value', ':value'))
            ->setMaxResults(1)
            ->setParameter('value', $value)
            ->setParameter('activated', true);
        try {
            $result = $qb->getQuery()->getSingleResult();
            return $result['email'];
        }
        catch(NoResultException $e) {
            return null;
        }
    }

    /**
     * @return integer
     */
    public function getNumberOfAllUsers()
    {
        $qb = $this->entityManager->createQueryBuilder();
        $qb->select('count(u.id)')
            ->from('ServiceUserBundle:User', 'u');
        $count = $qb->getQuery()->getSingleScalarResult();
        return $count;
    }

    /**
     * @param array $filters
     * @param bool $array
     * @return array
     */
    public function loadFiltered(array $filters, $array=false)
    {
        $user = $this->getUser();

        $location = $this->container->get('service.security')->getDefaultLocation();
        $account  = $user->getAccount();

        $qb = $this->entityManager->createQueryBuilder();
        $qb->select('u')
            ->from('ServiceUserBundle:User', 'u')
            ->where($qb->expr()->eq('u.account', ':account'))
            ->leftJoin('u.locations', 'l')
            ->andWhere($qb->expr()->eq('l', ':location'))
            ->andWhere('u.activated = :activated')
            ->setParameter('account', $account)
            ->setParameter('location', $location)
            ->setParameter('activated', true);

        if(isset($filters['max']))
        {
            $qb->setMaxResults($filters['max']);
        }
        else
        {
            $qb->setMaxResults(self::MAX_RESULTS);
        }

        $query = $qb->getQuery();
        $array ? $users = $query->getArrayResult() : $users = $query->getResult();

        if(!$array && isset($filters['role'])) // Roles cannot be checked with DQL, because they are defined in security.yml as a tree
        {
            $grantedUsers = array();

            foreach($users as $user)
            {
                /**
                 * @var User $user
                 */
                foreach($user->getReachableRoles($this->container->get('security.role_hierarchy')) as $role)
                {
                    /**
                     * @var Role $role
                     */
                    if($role->getRole() === $filters['role'])
                    {
                        $grantedUsers[] = $user;
                        break;
                    }
                }
            }
            return $grantedUsers;
        }

        return $users;
    }

    public function isEmailAvailable($email)
    {
        $qb = $this->entityManager->createQueryBuilder();
        $qb->select('count(u.email)')
            ->from('ServiceUserBundle:User', 'u')
            ->where($qb->expr()->eq('u.email', ':email'))
            ->setParameter('email', $email);

        $query = $qb->getQuery();
        $count = $query->getSingleScalarResult();

        return intval($count) === 0;
    }

    /**
     * @param $term
     * @param bool $onlyCurrentLocation
     * @return array
     */
    function loadByLike($term, $onlyCurrentLocation = false)
    {
        $account = $this->getUser()->getAccount();
        $terms = explode(' ', $term);

        $qb = $this->entityManager->createQueryBuilder();
        $qb->select('u')
            ->from('ServiceUserBundle:User', 'u')
            ->where('u.account = :account')
            ->andWhere('u.activated = 1')
            ->setParameter('account', $account);

        $i = 1;

        foreach($terms as $term)
        {
            $qb->andWhere(
                $qb->expr()->orX(
                    $qb->expr()->like('LOWER(u.firstName)', ':term' . $i),
                    $qb->expr()->like('LOWER(u.lastName)', ':term' . $i),
                    $qb->expr()->like('LOWER(u.email)', ':term' . $i)
                )
            )
                ->setParameter('term' . $i++, $this::like($term));
        }
        $qb->setMaxResults(20);

        if($onlyCurrentLocation)
        {
            $location = $this->container->get('service.security')->getDefaultLocation();
            $qb->andWhere(':location MEMBER OF u.locations')
               ->setParameter('location', $location);
        }

        try {
            $result = $qb->getQuery()->getArrayResult();
        }
        catch(NoResultException $e) {
            return [];
        };

        return $result;
    }
}
