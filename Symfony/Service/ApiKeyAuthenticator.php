<?php
/**
 * Created by PhpStorm.
 * User: tomaz
 * Date: 22/06/14
 * Time: 11:22
 */

namespace Service\Bundle\UserBundle\Security;

use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\SimplePreAuthenticatorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Authentication\Token\PreAuthenticatedToken;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;

class ApiKeyAuthenticator implements SimplePreAuthenticatorInterface, AuthenticationFailureHandlerInterface
{
    /**
     * @var ApiKeyUserProvider
     */
    protected $userProvider;

    /**
     * @var Router
     */
    protected $router;

    /**
     * @param ApiKeyUserProvider $userProvider
     * @param Router $router
     */
    public function __construct(ApiKeyUserProvider $userProvider, Router $router)
    {
        $this->userProvider = $userProvider;
        $this->router = $router;
    }

    public function createToken(Request $request, $providerKey)
    {
        if($request->headers->has('X-Auth-Token')) {
            $token = $request->headers->get('X-Auth-Token');
        }
        elseif($request->query->has('X-Auth-Token')) { # File downloads
            $token = $request->query->get('X-Auth-Token');
        }

        if(!isset($token)) {
            throw new BadCredentialsException("No API key present!", 401);
        }

        return new PreAuthenticatedToken(
            'anon.',
            $token,
            $providerKey
        );
    }

    public function authenticateToken(TokenInterface $token, UserProviderInterface $userProvider, $providerKey)
    {
        $tokenValue = $token->getCredentials();
        $username = $this->userProvider->getUsernameByToken($tokenValue);

        if (!$username) {
            throw new AuthenticationException(sprintf('X-Auth-Token "%s" is not valid!', $tokenValue), 401);
        }

        $user = $this->userProvider->loadUserByUsername($username);

        return new PreAuthenticatedToken(
            $user,
            $tokenValue,
            $providerKey,
            $user->getRoles()
        );
    }

    public function supportsToken(TokenInterface $token, $providerKey)
    {
        return $token instanceof PreAuthenticatedToken && $token->getProviderKey() === $providerKey;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        return new Response("Authentication failed. ".$exception->getMessage(), $exception->getCode());
    }
}