<?php
/**
 * Created by PhpStorm.
 * User: Tomaz
 * Date: 10.3.2014
 * Time: 9:42
 */

namespace Service\Bundle\BaseBundle\Form\Handler;

use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class FormHandler {

    /**
     * @var Form $form
     */
    protected $form;

    protected $request;

    protected $formFactory;

    public function __construct(FormFactory $formFactory, RequestStack $requestStack)
    {
        $this->formFactory = $formFactory;
        $this->request = $requestStack->getCurrentRequest();
    }

    public function process($formType, $entity=null, array $options=array(), $named=false)
    {
        if(!$named)
        {
            $this->form = $this->formFactory->createNamed(null, $formType, $entity, $options);
        }
        else
        {
            $this->form = $this->formFactory->create($formType, $entity, $options);
        }

        if ($this->request->getMethod() === 'POST')
        {
            $this->form->handleRequest($this->request);
            if ($this->form->isValid()) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param string $field
     * @return Form
     */
    public function getForm($field=null)
    {
        if($field === null)
        {
            return $this->form;
        }
        else
        {
            return $this->form[$field];
        }
    }

    /**
     * @return string
     */
    public function getFirstError()
    {
        foreach($this->getForm()->getErrors(true) as $error)
        {
            if($error instanceof FormError)
            {
                if($error->getOrigin() instanceof FormInterface)
                {
                    return 'Field: '.$error->getOrigin()->getName().', Message: '.$error->getMessage();
                }
                else
                {
                    return $error->getCause() . ' ' . $error->getMessage();
                }
            }
        }
        return 'Form contains no errors.';
    }
}