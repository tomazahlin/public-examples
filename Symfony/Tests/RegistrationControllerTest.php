<?php

namespace Service\Bundle\UserBundle\Tests\Controller;

use Service\Bundle\BaseBundle\Helper\ApiTestCaseHelper;

final class RegistrationControllerTest extends ApiTestCaseHelper
{
    /**
     * When user is authenticated, he can not post to register
     * @group account
     * @group user
     * @group email
     */
    public function testSuccessfulRegistration()
    {
        $data = array(
            'account' => ['name' => 'Test company'],
            'user' => ['email' => $userEmail = 'test@unexisting.com']
        );

        $response = $this->request('POST', 'service_register', [], $data, true, null, true);
        $this->assertSuccessfulResponse($response);

        // Check that an e-mail was sent
        $this->assertEquals(1, $this->getMailCollector()->getMessageCount());
        $this->assertEmail($this->getMail(), 'Registration', $userEmail, '/#/activate/');
    }

    /**
     * Tests bad registration data (invalid email, invalid company name, blank fields, existing email...)
     * @group account
     * @group user
     * @group email
     * @dataProvider getInvalidRegistrationData
     */
    public function testBadRegistration($data)
    {
        $response = $this->request('POST', 'service_register', [], $data, true, null, true);
        $this->assertForbiddenResponse($response);

        // Check that an e-mail was not sent
        $this->assertEquals(0, $this->getMailCollector()->getMessageCount());
    }

    /**
     * Test resend registration email
     * @group user
     * @group email
     */
    public function testResendRegistrationEmail()
    {
        $data = array('email' => $userEmail = 'unactivated@test.com');

        $response = $this->request('POST', 'service_register_resend', [], $data, true, null, true);
        $this->assertSuccessfulResponse($response);

        // Check that an e-mail was sent
        $this->assertEquals(1, $this->getMailCollector()->getMessageCount());
        $this->assertEmail($this->getMail(), 'Registration', $userEmail, '/#/activate/');
    }

    /**
     * Test resend registration email fail for user which is not unactivated
     * @group user
     * @group email
     */
    public function testResendRegistrationEmailForbiddenResponse()
    {
        $data = array('email' => $userEmail = 'activated@test.com');

        $response = $this->request('POST', 'service_register_resend', [], $data, true, null, true);
        $this->assertNotFoundResponse($response);

        // Check that an e-mail was not sent
        $this->assertEquals(0, $this->getMailCollector()->getMessageCount());
    }

    static function getInvalidRegistrationData()
    {
        return array(
            array('data' => array('user'    => array('email' => ''),
                                  'account' => array('name' => ''))
            ),
            array('data' => array('user'    => array('email' => 'test(at)test.com'),
                                  'account' => array('name' => 'Test'))
            ),
            array('data' => array('user'    => array('email' => 'existing@test.com'),
                                  'account' => array('name' => 'Test'))
            ),
            array('data' => array('user'    => array('email' => '@test.com'),
                                  'account' => array('name' => ''))
            ),
            array('data' => array('user'    => array('email' => 'test.new@test.com'),
                                  'account' => array('name' => '   '))
            ),
            array('data' => array('user'    => array('email' => 'test.new.2@test.com'),
                                  'account' => array('name' => 'a'))
            )
        );
    }
}