<?php
/**
 * Created by PhpStorm.
 * User: tomaz
 * Date: 10/05/14
 * Time: 20:35
 */

namespace Service\Bundle\BaseBundle\Helper;

use Service\Bundle\BaseBundle\Helper\Traits\Output;
use Swift_Message;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Bundle\SwiftmailerBundle\DataCollector\MessageDataCollector;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Profiler\Profile;
use Symfony\Component\Process\Exception\LogicException;

/**
 * Class ApiTestCaseHelper
 * Each test which extends this helper class should be annotated with group
 * Groups: account, user, group, customer, repair, sale, location, service, supply.
 */
abstract class ApiTestCaseHelper extends WebTestCase
{
    use Output;

    /**
     * Api token
     * @var string
     */
    protected $token;

    /**
     * @var integer
     */
    protected $uid;

    /**
     * @var integer
     */
    protected $aid;

    /**
     * @var Profile
     */
    protected $profile;

    public function setUp()
    {
        parent::setUp();
        $this->printClassName();
    }

    /**
     * Use this method to login the user. It sets the token property.
     */
    protected function login()
    {
        $data = array(
            '_username' => 'existing@test.com',
            '_password' => 'password'
        );

        $response = $this->request('POST', 'login_check', [], $data, false);

        $this->assertTrue($response->isSuccessful());

        $content = json_decode($response->getContent()); // Returns stdClass

        $this->token = $content->token;
        $this->uid = $content->userId;
        $this->aid = $content->accountId;
    }

    protected function request($method, $routeId, array $routeParams = array(), array $data = array(), $addToken = true, $token = null, $enableProfiler = false, $ob = false)
    {
        if($token === null) $token = $this->token;

        $addToken ? $server = array('HTTP_X-Auth-Token' => $token) : $server = [];

        $client = static::createClient();

        $this->profile = null;

        if($enableProfiler)
        {
            $client->enableProfiler();
        }

        if($ob) {
            ob_start(); // Prevent echoing the output in console
        }

        $client->request($method, $this->getRoute($routeId, $routeParams), $data, array(), $server);

        if($ob) {
            ob_end_clean();
        }

        $this->profile = $client->getProfile();

        return $client->getResponse();
    }

    protected function logout()
    {
        $response = $this->request('DELETE', 'logout', [], []);
        $this->assertTrue($response->isSuccessful());
        $this->token = null;
        $this->uid = null;
        $this->aid = null;
    }

    protected function getContainer()
    {
        $client = static::createClient();
        return $client->getContainer();
    }

    protected function getRoute($routeId, array $parameters = array())
    {
        $client = static::createClient();
        $loginRoute = $client->getContainer()->get('router')->generate($routeId, $parameters);
        return $loginRoute;
    }

    protected function decode(Response $response, $extractKey = null)
    {
        $this->assertResponseJson($response);

        $data = json_decode($response->getContent(),true);

        if($extractKey === null)
        {
            return $data;
        }
        else
        {
            $keys = array();

            foreach($data as $d)
            {
                $keys[] = $d[$extractKey];
            }

            return $keys;
        }
    }

    /**
     * @return Profile
     * @throws \LogicException
     */
    protected function getProfile()
    {
        if($this->profile instanceof Profile)
        {
            return $this->profile;
        }
        throw new LogicException('Profiler was not enabled during request!');
    }

    /**
     * @return MessageDataCollector
     */
    protected function getMailCollector()
    {
        return $this->getProfile()->getCollector('swiftmailer');
    }

    /**
     * @param int $n
     * @return Swift_Message
     * @throws LogicException
     */
    protected function getMail($n=0)
    {
        $messages =  $this->getMailCollector()->getMessages();

        if(isset($messages[$n]))
        {
            $this->assertInstanceOf('Swift_Message', $messages[$n]);
            return $messages[$n];
        }
        throw new LogicException('Message not found!');
    }
    # ASSERTIONS

    final protected function assertResponseJson(Response $response)
    {
        $this->assertTrue($response->headers->has('content_type'));

        $this->assertEquals('application/json', $response->headers->get('content_type'));
    }

    final protected function assertArrayHasMultidimensionalKeys(array $data, array $keys)
    {
        foreach($keys as $k => $v)
        {
            is_numeric($k) ? $key = $v : $key = $k;

            $this->assertArrayHasKey($key, $data);

            if(is_array($v)) {
                $this->assertArrayHasMultidimensionalKeys($data[$k], $v);
            }
        }
    }

    final protected function assertSuccessfulResponseWithManyItems(Response $response, array $arrayKeys = array('id'))
    {
        $this->assertTrue($response->isSuccessful());

        $this->assertResponseJson($response);

        $data = json_decode($response->getContent(),true);

        $this->assertNotNull($data); # Json is decoded successfully

        $this->assertGreaterThan(0, count($data));

        foreach($arrayKeys as $key)
        {
            $this->assertArrayHasKey($key, array_values($data)[0]);
        }
    }

    final protected function assertSuccessfulResponseWithOneItem(Response $response, array $arrayKeys = array('id'))
    {
        $this->assertTrue($response->isSuccessful());

        $this->assertResponseJson($response);

        $data = json_decode($response->getContent(),true);

        $this->assertNotNull($data); # Json is decoded successfully

        foreach($arrayKeys as $key)
        {
            $this->assertArrayHasKey($key, $data);
        }
    }

    final protected function assertSuccessfulResponseWithZeroOrManyItems(Response $response, array $arrayKeys = array('id'), $maxResults = null)
    {
        $this->assertTrue($response->isSuccessful());

        $this->assertResponseJson($response);

        $data = json_decode($response->getContent(),true);

        $this->assertNotNull($data); # Json is decoded successfully

        if($maxResults !== null)
        {
            $this->assertLessThanOrEqual($maxResults, count($data));
        }

        if(count($data))
        {
            foreach($arrayKeys as $key)
            {
                $this->assertArrayHasKey($key, array_values($data)[0]);
            }
        }
    }

    final protected function assertSuccessfulResponse(Response $response)
    {
        $this->assertTrue($response->isSuccessful());
    }

    final protected function assertNotFoundResponse(Response $response)
    {
        $this->assertTrue($response->isNotFound());
    }

    final protected function assertForbiddenResponse(Response $response)
    {
        $this->assertTrue($response->isForbidden());
    }

    final protected function assertUnauthorizedResponse(Response $response)
    {
        $this->assertTrue($response->getStatusCode() === Response::HTTP_UNAUTHORIZED);
    }

    final protected function assertCreatedResponse(Response $response)
    {
        $this->assertTrue($response->getStatusCode() === Response::HTTP_CREATED);
    }

    final protected function assertPdfResponse(Response $response)
    {
        $this->assertEquals('application/pdf', $response->headers->get('Content-Type'));
    }

    final protected function assertContentType(Response $response, $contentType)
    {
        $this->assertContains($contentType, $response->headers->get('Content-Type'));
    }

    final protected function assertAttachmentResponse(Response $response)
    {
        $this->assertContains('attachment', $response->headers->get('Content-Disposition'));
    }

    final protected function assertNotAttachmentResponse(Response $response)
    {
        if($response->headers->has('Content-Disposition'))
        {
            $this->assertNotContains('attachment', $response->headers->get('Content-Disposition'));
        }
    }

    final protected function assertEmail(Swift_Message $email, $subject, $receiver, $contentNeedle)
    {
        $this->assertEquals($subject, $email->getSubject());
        $this->assertEquals($receiver, key($email->getTo()));
        $this->assertContains($contentNeedle, $email->getBody());
    }

    final protected function isOnline()
    {
        return @fsockopen("www.google.com", 80, $errno, $errstr, 5) === true;
    }
} 