<?php

namespace Service\Bundle\UserBundle\Tests\Controller;

use Service\Bundle\BaseBundle\Helper\ApiTestCaseHelper;

final class GroupControllerTest extends ApiTestCaseHelper
{
    /**
     * @before
     */
    public function login()
    {
        parent::login();
    }

    /**
     * @after
     */
    public function logout()
    {
        parent::logout();
    }

    /**
     * Test group creation
     * Tests that the api returns a successful created response
     * @group group
     * @dataProvider getGroupValidData
     */
    public function testNewGroupResponse($data)
    {
        $response = $this->request('POST', 'service_group_new', [], $data);
        $this->assertCreatedResponse($response);
    }

    /**
     * Test group creation fail
     * Tests that the api returns a forbidden response
     * @group group
     * @dataProvider getGroupInvalidData
     */
    public function testNewGroupForbiddenResponse($data)
    {
        $response = $this->request('POST', 'service_group_new', [], $data);
        $this->assertForbiddenResponse($response);
    }

    /**
     * Test group update
     * Tests that the api returns a successful created response
     * @group group
     * @dataProvider getGroupValidData
     */
    public function testUpdateGroupResponse($data)
    {
        $response = $this->request('POST', 'service_group_update', ['id' => 5], $data);
        $this->assertSuccessfulResponse($response);
    }

    /**
     * Test group update fail
     * Tests that the api returns a forbidden response
     * @group group
     * @dataProvider getGroupInvalidData
     */
    public function testUpdateGroupForbiddenResponse($data)
    {
        $response = $this->request('POST', 'service_group_update', ['id' => 5], $data);
        $this->assertForbiddenResponse($response);
    }

    /**
     * Test empty group removal
     * Tests that the api returns a successful response
     * @group group
     */
    public function testEmptyGroupRemovalResponse()
    {
        # Create group first
        $data = self::getGroupValidData()[0]['data'];
        $response = $this->request('POST', 'service_group_new', [], $data);
        $this->assertCreatedResponse($response);
        $id = $this->decode($response)['id'];

        $response = $this->request('DELETE', 'service_group_remove', ['id' => $id]);
        $this->assertSuccessfulResponse($response);
    }

    /**
     * Test general group removal fail
     * Tests that the api returns a forbidden response
     * @group group
     */
    public function testGeneralGroupRemovalForbiddenResponse()
    {
        $response = $this->request('DELETE', 'service_group_remove', ['id' => 2]);
        $this->assertForbiddenResponse($response);
    }

    /**
     * Get available groups test
     * Tests that the api returns a successful response
     * @group group
     */
    public function testGetGroupsResponse()
    {
        $response = $this->request('GET', 'service_groups_get');
        $this->assertSuccessfulResponseWithManyItems($response);
    }

    /**
     * Get available general group test
     * Tests that the api returns a successful response
     * @group group
     */
    public function testGetGroupResponse()
    {
        $response = $this->request('GET', 'service_group_get', ['id' => 1]);
        $this->assertSuccessfulResponseWithOneItem($response);
    }

    /**
     * Get available custom group test
     * Tests that the api returns a successful response
     * @group group
     */
    public function testGetCustomGroupResponse()
    {
        $response = $this->request('GET', 'service_group_get', ['id' => 5]);
        $this->assertSuccessfulResponseWithOneItem($response);
    }

    /**
     * Get forbidden custom group test
     * Tests that the api returns a forbidden response
     * @group group
     */
    public function testGetCustomGroupForbiddenResponse()
    {
        $response = $this->request('GET', 'service_group_get', ['id' => 6]);
        $this->assertForbiddenResponse($response);
    }

    # DATA PROVIDERS

    static function getGroupValidData()
    {
        return array(
            array('data' => array('name' => 'Test group',
                                  'roles' => array(8, 9, 10, 11, 13))
            ),
            array('data' => array('name' => 'Another test group',
                                  'roles' => array(10))
            )
        );
    }

    static function getGroupInvalidData()
    {
        return array(
            array('data' => array('name' => '',
                                  'roles' => array(8, 9, 10, 11, 13))
            ),
            array('data' => array('name' => 'T',
                                  'roles' => array(10))
            ),
            array('data' => array('name' => 'Test group',
                                  'roles' => array())
            ),
            array('data' => array('name' => 'Another test group',
                                  'roles' => array('Something bad'))
            )
        );
    }
}