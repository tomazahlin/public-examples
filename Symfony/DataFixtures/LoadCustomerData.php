<?php
/**
 * Created by PhpStorm.
 * User: tomaz
 * Date: 21/05/14
 * Time: 17:09
 */

namespace Service\Bundle\CustomerBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Service\Bundle\BaseBundle\DataFixtures\DataCollection;
use Service\Bundle\CustomerBundle\Entity\Customer;
use Service\Bundle\CustomerBundle\Entity\CustomerStatus;

class LoadCustomerData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $limit = 200;
        $dc = new DataCollection();

        $firstNames         = $dc->getRandom('firstNames', 1982092, $limit);
        $lastNames          = $dc->getRandom('lastNames', 3198865, $limit);
        $telephoneNumbers   = $dc->getRandom('telephoneNumbers', 2186409, $limit);
        $companyNames       = $dc->getRandom('companyNames', 5109089, $limit);
        $taxNumbers         = $dc->getRandom('taxNumbers', 2123970, $limit);
        $addresses          = $dc->getRandom('addresses', 3359652, $limit);
        $addressesSecond    = $dc->getRandom('addresses', 7629682, $limit);
        $cities             = $dc->getRandom('cities', 2778711, $limit);
        $zipCodes           = $dc->getRandom('zipCodes', 1386339, $limit);
        $states             = $dc->getRandom('states', 2108855, $limit);
        $countries          = $dc->getRandom('countries', 3008722, $limit);
        $customerStatuses   = $dc->getRandom('customerStatuses', 1332364, $limit);

        $accounts = $dc->get('accounts');

        for($i=0; $i<$limit; $i++)
        {
            $email = strtolower($firstNames[$i]).'.'.strtolower($lastNames[$i]).'@gmail.com';

            $customerStatus = new CustomerStatus();
            $customerStatus->setName($customerStatuses[$i]['name'])
                           ->setPriority($customerStatuses[$i]['priority'])
                           ->setDiscount($customerStatuses[$i]['discount']);

            $customer = new Customer();
            $customer->setFirstName($firstNames[$i])
                     ->setLastName($lastNames[$i])
                     ->setEmail($email)
                     ->setTelephone($telephoneNumbers[$i])
                     ->setTaxNumber($taxNumbers[$i])
                     ->setTaxPayer(true)
                     ->setAddress($addresses[$i])
                     ->setCity($cities[$i])
                     ->setZipCode($zipCodes[$i])
                     ->setState($states[$i])
                     ->setCountry($countries[$i])
                     ->setStatus($customerStatus)
                     ->setAccount($this->getReference($accounts[$i%count($accounts)]));

            $this->setReference('customer-'.$i, $customer);

            if($i%9 == 0)
            {
                $customer->setAddressSecond($addressesSecond[$i]);
            }

            if($i%11 == 0)
            {
                $customer->setCompanyName($companyNames[$i]);
            }

            $manager->persist($customer);
            $manager->flush();
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 206; // the order in which fixtures will be loaded
    }
}