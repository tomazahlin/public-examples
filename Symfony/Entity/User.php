<?php

namespace Service\Bundle\UserBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;

use Service\Bundle\BaseBundle\Entity\Interfaces\ArrayInterface;
use Service\Bundle\BaseBundle\Entity\Person;
use Service\Bundle\BaseBundle\Entity\Traits\ArrayTrait;
use Service\Bundle\BaseBundle\Entity\Traits\Log;
use Service\Bundle\BaseBundle\Helper\Text;
use Service\Bundle\RepairBundle\Entity\Repair;
use Service\Bundle\RepairBundle\Entity\Sale;
use Service\Bundle\RepairBundle\Entity\ServiceRepairArchive;
use Service\Bundle\UserBundle\Entity\Interfaces\ActivationInterface;
use Service\Bundle\UserBundle\Entity\Interfaces\ActivationTokenInterface;
use Service\Bundle\UserBundle\Entity\Traits\Activation;
use Service\Bundle\UserBundle\Entity\Traits\ActivationToken;
use Service\Bundle\UserBundle\Security\Sha512SaltEncoder;
use Symfony\Component\Security\Core\Role\RoleHierarchy;
use Symfony\Component\Security\Core\User\UserInterface;

use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\MaxDepth;

/**
 * User
 * @ExclusionPolicy("none")
 */
class User extends Person implements ActivationInterface, ActivationTokenInterface, UserInterface, \Serializable, ArrayInterface
{
    use Activation;
    use ActivationToken;

    use Log;

    use ArrayTrait;

    /**
     * @var boolean
     */
    private $main;

    /**
     * @Exclude
     * @var string
     */
    private $salt;

    /**
     * @Exclude
     * @var string
     */
    private $passwordPlain;

    /**
     * @Exclude
     * @var string
     */
    private $password;

    /**
     * @Exclude
     * @var string
     */
    private $passwordRepeat;

    /**
     * @Exclude
     * @var string
     */
    private $passwordResetToken;

    /**
     * @var Group
     */
    private $group;

    /**
     * @Exclude
     * @var ArrayCollection
     */
    private $locations;

    /**
     * @Exclude
     * @var Location
     */
    private $primaryLocation;

    /**
     * Added repairs
     * @Exclude
     * @var ArrayCollection
     */
    private $repairs;

    /**
     * Working on repairs (assigned as main repairman)
     * @Exclude
     * @var ArrayCollection
     */
    private $workingRepairs;

    /**
     * @Exclude
     * @var ArrayCollection
     */
    private $sales;

    /**
     * @Exclude
     * @var ArrayCollection
     */
    private $tokens;

    /**
     * Following information (logs) on repair
     * @Exclude
     * @var ArrayCollection
     */
    private $followingRepairs;

    /**
     * @var string
     */
    private $stripeId;

    /**
     * @Exclude
     * @var ArrayCollection
     */
    private $payments;

    /**
     * @Exclude
     * @var ArrayCollection
     */
    private $tasks;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->locations = new ArrayCollection();
        $this->repairs = new ArrayCollection();
        $this->sales = new ArrayCollection();
        $this->workingRepairs = new ArrayCollection();
        $this->followingRepairs = new ArrayCollection();
        $this->tokens = new ArrayCollection();
        $this->logs = new ArrayCollection();
        $this->payments = new ArrayCollection();
        $this->tasks = new ArrayCollection();
    }

    /**
     * @param $type
     * @return User
     */
    public function log($type)
    {
        $log = new UserLog();
        $log->setType($type)->setUser($this);
        $this->addLog($log);
        return $this;
    }

    /**
     * @JMS\Serializer\Annotation\VirtualProperty
     * @JMS\Serializer\Annotation\SerializedName("primaryLocationId")
     * @return integer
     */
    public function getPrimaryLocationId()
    {
        return $this->getPrimaryLocation()->getId();
    }

    /**
     * Remove token value (called upon logout)
     *
     * @param $value
     * @return boolean
     */
    public function removeTokenValue($value)
    {
        $removed = false;

        foreach($this->tokens as $token)
        {
            if($token instanceof Token)
            {
                if($token->getValue() === $value)
                {
                    $removed = $this->tokens->removeElement($token);
                    break;
                }
            }
        }
        return $removed;
    }

    /**
     * @return Token
     */
    public function generateToken()
    {
        $token = new Token();
        $token->setUser($this)
              ->generateValue();
        $this->tokens->add($token);
        return $token;
    }

    /**
     * Set main
     *
     * @param boolean $main
     * @return User
     */
    public function setMain($main)
    {
        $this->main = $main;

        return $this;
    }

    /**
     * Get main
     *
     * @return boolean
     */
    public function getMain()
    {
        return $this->main;
    }

    /**
     * Is main (Alias method for get main)
     *
     * @return boolean
     */
    public function isMain()
    {
        return $this->getMain();
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return User
     */
    public function setFirstName($firstName)
    {
        parent::setFirstName($firstName);
        return $this;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return User
     */
    public function setLastName($lastName)
    {
        parent::setLastName($lastName);

        return $this;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        parent::setEmail($email);
        return $this;
    }

    public function setUsername($email)
    {
        return $this->setEmail($email);
    }

    public function getUsername()
    {
        return $this->getEmail();
    }
    /**
     * Generate salt
     *
     * @param
     * @return User
     */
    private function generateSalt()
    {
        $this->salt = Text::generateRandom(60);

        return $this;
    }

    /**
     * Get salt
     *
     * @return string 
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Set password (input is plain password)
     * sha1 returns 40 character string
     *
     * @param string $password
     * @return User
     */
    public function setPasswordPlain($password)
    {
        $this->passwordPlain = $password;
        $this->setPassword($password);
        return $this;
    }

    /**
     * Get password plain
     *
     * @return string
     */
    public function getPasswordPlain()
    {
        return $this->passwordPlain;
    }

    public function setPassword($password)
    {
        $this->generateSalt();
        $this->password = Sha512SaltEncoder::encode($password,$this->salt);
        return $this;
    }

    public function hasPassword()
    {
        return $this->password !== null;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set password repeat (input is plain password)
     *
     * @param string $password
     * @return User
     */
    public function setPasswordRepeat($password)
    {
        $this->passwordRepeat = $password;
        return $this;
    }

    /**
     * Get password repeat
     *
     * @return string 
     */
    public function getPasswordRepeat()
    {
        return $this->passwordRepeat;
    }

    /**
     * Check if password is typed the same in both fields
     * @return bool
     */
    public function isPasswordRepeated()
    {
        return $this->passwordPlain === $this->passwordRepeat;
    }

    /**
     * Generate passwordResetToken
     *
     * @return User
     */
    public function generatePasswordResetToken()
    {
        $this->passwordResetToken = Text::generateRandom(60);

        return $this;
    }

    /**
     * Remove passwordResetToken
     *
     * @return User
     */
    public function removePasswordResetToken()
    {
        $this->passwordResetToken = null;

        return $this;
    }

    /**
     * Get passwordResetToken
     *
     * @return string 
     */
    public function getPasswordResetToken()
    {
        return $this->passwordResetToken;
    }

    /**
     * Erases sensitive data, such as password
     * @return $this
     */
    public function eraseCredentials()
    {
        $this->passwordPlain = null;
        return $this;
    }

    /**
     * Set group
     *
     * @param Group $group
     * @return User
     */
    public function setGroup(Group $group = null)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group
     *
     * @return Group
     */
    public function getGroup()
    {
        return $this->group;
    }

    public function hasGroup(Group $group)
    {
        return $group === $this->getGroup();
    }

    /**
     * Get user roles as strings in array
     * @return array
     */
    public function getRoles()
    {
        $roles = $this->getGroup()->getRoles()->toArray();
        $roles_str = [];
        foreach($roles as $role)
        {
            if($role instanceof Role) {
                $roles_str[] = $role->getRole();
            }
        }
        return $roles_str;
    }

    /**
     * Get reachable user roles as strings in array
     * @param RoleHierarchy $rh
     * @return array
     */
    public function getReachableRoles(RoleHierarchy $rh)
    {
        return $rh->getReachableRoles($this->getGroup()->getRoles()->toArray());
    }

    /**
     * Set account
     *
     * @param Account $account
     * @return User
     */
    public function setAccount(Account $account)
    {
        parent::setAccount($account);
        return $this;
    }

    ###################
    #### LOCATIONS ####
    ###################

    /**
     * Add location
     * @param Location $location
     * @return User
     */
    public function addLocation(Location $location)
    {
        $this->locations->add($location);
        $location->addUser($this);
        $this->getAccount()->addLocation($location);
        return $this;
    }

    /**
     * Has location
     * @param Location $location
     * @return bool
     */
    public function hasLocation(Location $location)
    {
        return $this->locations->contains($location);
    }

    /**
     * Get Locations
     * @return ArrayCollection
     */
    public function getLocations()
    {
        return $this->locations;
    }

    /**
     * Set Locations
     * @param ArrayCollection $locations
     * @return User
     */
    public function setLocations(ArrayCollection $locations = null)
    {
        $this->locations = $locations;
        return $this;
    }

    /**
     * Set primary Location
     * @param Location $location
     * @return User
     */
    public function setPrimaryLocation(Location $location = null)
    {
        $location->setAccount($this->getAccount());
        $this->primaryLocation = $location;
        return $this;
    }

    /**
     * Get primary Location
     * @return Location
     */
    public function getPrimaryLocation()
    {
        return $this->primaryLocation;
    }

    /**
     * Has primary Location
     * @return Location
     */
    public function hasPrimaryLocation()
    {
        return $this->getPrimaryLocation() !== null;
    }

    /**
     * Check if primary location is in user locations
     * @return bool
     */
    public function isPrimaryLocationMatch()
    {
        return $this->getLocations()->contains($this->getPrimaryLocation());
    }

    ###############
    ### REPAIRS ###
    ###############

    /**
     * Add repair
     * @param Repair $repair
     * @return User
     */
    public function addRepair(Repair $repair)
    {
        $repair->setUser($this);
        $this->repairs->add($repair);
        return $this;
    }

    /**
     * Remove repair
     * @param Repair $repair
     * @return User
     */
    public function removeRepair(Repair $repair)
    {
        $this->repairs->removeElement($repair);
        return $this;
    }

    /**
     * @param Repair $repair
     * @return bool
     */
    public function hasRepair(Repair $repair)
    {
        return $this->repairs->contains($repair);
    }

    /**
     * Get Repairs
     * @return ArrayCollection
     */
    public function getRepairs()
    {
        return $this->repairs;
    }

    /**
     * @param ArrayCollection $repairs
     * @return User
     */
    public function setRepairs(ArrayCollection $repairs)
    {
        $this->repairs = $repairs;
        return $this;
    }

    /**
     * @param ArrayCollection $repairs
     * @return User
     */
    public function setWorkingRepairs(ArrayCollection $repairs)
    {
        $this->workingRepairs = $repairs;
        return $this;
    }

    /**
     * Get Working Repairs
     * @return ArrayCollection
     */
    public function getWorkingRepairs()
    {
        return $this->workingRepairs;
    }

    ###############
    #### SALES ####
    ###############

    /**
     * Add sale
     * @param Sale $sale
     * @return User
     */
    public function addSale(Sale $sale)
    {
        $sale->setUser($this);
        $this->sales->add($sale);
        return $this;
    }

    /**
     * Remove sale
     * @param Sale $sale
     * @return User
     */
    public function removeSale(Sale $sale)
    {
        $this->sales->removeElement($sale);
        return $this;
    }

    /**
     * @param Sale $sale
     * @return bool
     */
    public function hasSale(Sale $sale)
    {
        return $this->sales->contains($sale);
    }

    /**
     * Get sales
     * @return ArrayCollection
     */
    public function getSales()
    {
        return $this->sales;
    }

    /**
     * @param ArrayCollection $sales
     * @return User
     */
    public function setSales(ArrayCollection $sales)
    {
        $this->sales = $sales;
        return $this;
    }

    ###############
    ## FOLLOWING ##
    ## REPAIRS   ##
    ###############

    /**
     * Follow repair
     * @param Repair $repair
     * @return User
     */
    public function followRepair(Repair $repair)
    {
        $repair->setUser($this);
        $this->followingRepairs->add($repair);
        return $this;
    }

    /**
     * Unfollow repair
     * @param Repair $repair
     * @return User
     */
    public function unfollowRepair(Repair $repair)
    {
        $this->followingRepairs->removeElement($repair);
        return $this;
    }

    /**
     * @param Repair $repair
     * @return bool
     */
    public function followsRepair(Repair $repair)
    {
        return $this->followingRepairs->contains($repair);
    }

    /**
     * Get following repairs
     * @return ArrayCollection
     */
    public function getFollowingRepairs()
    {
        return $this->followingRepairs;
    }

    /**
     * @param ArrayCollection $repairs
     * @return User
     */
    public function setFollowingRepairs(ArrayCollection $repairs)
    {
        $this->followingRepairs = $repairs;
        return $this;
    }

    /**
     * Serialize the user.
     *
     * The serialized data have to contain the fields used by the equals method and the username.
     *
     * @return string
     */
    public function serialize()
    {
        return serialize(array(
            $this->getPassword(),
            $this->getSalt(),
            $this->getEmail(),
            $this->getActivated(),
            $this->getId()
        ));
    }

    /**
     * Unserialize the user.
     *
     * @param string $serialized
     */
    public function unserialize($serialized)
    {
        $data = unserialize($serialized);
        // add a few extra elements in the array to ensure that we have enough keys when unserializing
        // older data which does not include all properties.
        # $data = array_merge($data, array_fill(0, 2, null));

        list(
            $this->password,
            $this->salt,
            $this->email,
            $this->activated,
            $this->id
            ) = $data;
    }

    # STRIPE

    /**
     * Set stripe customer id
     * @param string $id
     * @return User
     */
    public function setStripeId($id)
    {
        $this->stripeId = $id;
        return $this;
    }

    /**
     * Set stripe customer id
     * @return string
     */
    public function getStripeId()
    {
        return $this->stripeId;
    }

    /**
     * @return bool
     */
    public function hasStripeId()
    {
        return $this->stripeId !== null;
    }

    # PAYMENTS

    /**
     * @param bool $onlyPaid
     * @return ArrayCollection
     */
    public function getPayments($onlyPaid=false)
    {
        if($onlyPaid)
        {
            $criteria = Criteria::create()
                ->where(Criteria::expr()->eq('paid', true));
            return $this->payments->matching($criteria);
        }
        return $this->payments;
    }

    # TASKS

    /**
     * @param ServiceRepairArchive $task
     * @return User
     */
    public function addTask(ServiceRepairArchive $task)
    {
        if(!$this->tasks->contains($task))
        {
            $task->setUser($this);
            $this->tasks->add($task);
        }
        return $this;
    }

    /**
     * @param ServiceRepairArchive $task
     * @return User
     */
    public function removeTask(ServiceRepairArchive $task)
    {
        $task->removeUser();
        $this->tasks->removeElement($task);
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getTasks()
    {
        return $this->tasks;
    }
}