<?php

namespace Service\Bundle\RepairBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Service\Bundle\BaseBundle\Entity\Interfaces\CommentInterface;
use Service\Bundle\BaseBundle\Entity\Time;
use Service\Bundle\BaseBundle\Entity\Traits\Comment;
use Service\Bundle\BaseBundle\Entity\Traits\Log;
use Service\Bundle\RepairBundle\Entity\Traits\Quantity;
use Service\Bundle\RepairBundle\Entity\Traits\Tax;
use Service\Bundle\RepairBundle\Entity\Traits\Value;
use Service\Bundle\UserBundle\Entity\Location;
use Service\Bundle\UserBundle\Entity\User;

use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\MaxDepth;

/**
 * @ExclusionPolicy("none")
 * Supply
 */
class Supply extends Time implements CommentInterface
{
    const MARGIN = 0.0005;

    use Comment;

    use Log;

    /**
     * @var integer
     */
    private $id;

    /**
     * @Exclude
     * @var ArrayCollection
     */
    private $inventories;

    /**
     * @Exclude
     * @var ArrayCollection
     */
    private $inventoriesCopy;
    
    /**
     * @var Supplier
     */
    private $supplier;

    /**
     * @Exclude
     * @var Location
     */
    private $location;

    /**
     * @var \DateTime
     */
    protected $timeReceived;

    /**
     * @var float
     */
    private $shippingCost = 0.0;

    /**
     * @var float
     */
    private $otherCost = 0.0;

    /**
     * @var int
     */
    private $numOfServices = 0;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->inventories      = new ArrayCollection();
        $this->inventoriesCopy  = new ArrayCollection();
        $this->logs             = new ArrayCollection();
    }

    /**
     * @param User $user
     * @param $type
     * @return Sale
     */
    public function log(User $user, $type)
    {
        $log = new SupplyLog();
        $log->setType($type)->setUser($user)->setSupply($this);
        $this->addLog($log);
        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param \DateTime $timeReceived
     * @return Supply
     */
    public function setTimeReceived(\DateTime $timeReceived)
    {
        $this->timeReceived = $timeReceived;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getTimeReceived()
    {
        return $this->timeReceived;
    }
    
    # SUPPLIER

    /**
     * Set Supplier
     *
     * @param Supplier $supplier
     * @return Supply
     */
    public function setSupplier(Supplier $supplier)
    {
        $this->supplier = $supplier;

        return $this;
    }

    /**
     * Get Supplier
     *
     * @return Supplier
     */
    public function getSupplier()
    {
        if($this->supplier === null)
        {
            $this->supplier = new Supplier();
        }
        return $this->supplier;
    }
    
    # LOCATION
    
    /**
     * Set Location
     *
     * @param Location $location
     * @return Supply
     */
    public function setLocation(Location $location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get Location
     *
     * @return Location
     */
    public function getLocation()
    {
        return $this->location;
    }

    ###############
    ## INVENTORY ##
    ###############

    /**
     * Add inventory
     * @param Inventory $inventory
     * @return Supply
     */
    public function addInventory(Inventory $inventory)
    {
        $inventory->setSupply($this);
        $this->inventories->add($inventory);
        return $this;
    }

    /**
     * Remove inventory
     * @param Inventory $inventory
     * @return Supply
     */
    public function removeInventory(Inventory $inventory)
    {
        $this->inventories->removeElement($inventory);
        return $this;
    }

    /**
     * @param Inventory $inventory
     * @return bool
     */
    public function hasInventory(Inventory $inventory)
    {
        return $this->inventories->contains($inventory);
    }

    /**
     * Get Inventories
     * @return ArrayCollection
     */
    public function getInventories()
    {
        return $this->inventories;
    }

    /**
     * @param ArrayCollection $inventories
     * @return Supply
     */
    public function setInventories(ArrayCollection $inventories)
    {
        $this->inventories = $inventories;
        return $this;
    }

    /**
     * Get Inventories copy
     * @return ArrayCollection
     */
    public function getInventoriesCopy()
    {
        if(!($this->inventoriesCopy instanceof ArrayCollection))
        {
            $this->inventoriesCopy = new ArrayCollection(); // For postLoad event
        }
        return $this->inventoriesCopy;
    }

    /**
     * Add to copy inventory
     * @param Inventory $inventory
     * @return Supply
     */
    public function addInventoryCopy(Inventory $inventory)
    {
        $inv = clone $inventory;
        $this->getInventoriesCopy()->add($inv);
        return $this;
    }

    public function copyInventory() {
        foreach($this->getInventories() as $inventory)
        {
            $this->addInventoryCopy($inventory);
        }
        return $this;
    }
    #########
    # COSTS #
    #########

    /**
     * @param $value
     * @return Supply
     */
    public function setShippingCost($value)
    {
        $this->shippingCost = $value;

        return $this;
    }

    /**
     * @return float
     */
    public function getShippingCost()
    {
        return $this->shippingCost;
    }

    /**
     * @param $value
     * @return Supply
     */
    public function setOtherCost($value)
    {
        $this->otherCost = $value;

        return $this;
    }

    /**
     * @return float
     */
    public function getOtherCost()
    {
        return $this->otherCost;
    }

    /**
     * @return float
     */
    public function getExtraCosts()
    {
        return $this->getShippingCost() + $this->getOtherCost();
    }

    #########

    /**
     * @param int $num
     * @return Supply
     */
    public function setNumOfServices($num)
    {
        $this->numOfServices = $num;
        return $this;
    }

    /**
     * @return int
     */
    public function getNumOfServices()
    {
        return $this->numOfServices;
    }

    # Price

    /**
     * @param string $attr
     * @return float
     */
    protected function getSumOf($attr)
    {
        $sum = 0.0;

        foreach($this->getInventories() as $inventory)
        {
            if($inventory instanceof Inventory)
            {
                $value = $inventory->getValue() * $inventory->getQuantity();

                switch($attr) {
                    case 'value':
                        $sum += $value;
                        break;
                    case 'tax':
                        $sum += $inventory->getTax(true)*$value; # Has no discount
                        break;
                }

            }
        }
        return $sum;
    }

    /**
     * @JMS\Serializer\Annotation\VirtualProperty
     * @JMS\Serializer\Annotation\SerializedName("baseValue")
     * @return float
     */
    public function getBaseValue()
    {
        return $this->getSumOf('value');
    }

    /**
     * @JMS\Serializer\Annotation\VirtualProperty
     * @JMS\Serializer\Annotation\SerializedName("taxValue")
     * @return float
     */
    public function getTaxValue()
    {
        return $this->getSumOf('tax');
    }

    /**
     * @JMS\Serializer\Annotation\VirtualProperty
     * @JMS\Serializer\Annotation\SerializedName("finalValue")
     * @return float
     */
    public function getFinalValue()
    {
        return $this->getBaseValue()+$this->getTaxValue();
    }
}
