<?php
/**
 * Created by PhpStorm.
 * User: Tomaz
 * Date: 17.12.2013
 * Time: 13:38
 */

namespace Service\Bundle\BaseBundle\Entity;

/* For every type of file (logo,image,document,attachment...) make a new class and extend this class */
/* Also implement the interface on the extended files */

abstract class ImageFile extends File
{
    public function getAllowedMimeTypes()
    {
        return array(
            'image/gif',
            'image/jpeg',
            'image/png'
        );
    }
}