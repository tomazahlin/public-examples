<?php

namespace Service\Bundle\UserBundle\Entity;

use Gregwar\Image\Image;
use Service\Bundle\BaseBundle\Entity\ImageFile;

/**
 * Logo file
 */
class LogoFile extends ImageFile
{
    const FULL_WIDTH = 300;
    const FULL_HEIGHT = 300;

    const THUMB_WIDTH = 60;
    const THUMB_HEIGHT = 60;

    /**
     * @var Account
     */
    private $account;

    function __construct(Account $account)
    {
        $this->account = $account;
    }

    public function getUploadDirectory()
    {
        return 'files/account/'.$this->account->getId().'/logo';
    }

    public function getThumbnailDirectory()
    {
        return $this->getFullDirectory() . '/thumb';
    }

    protected function postUpload()
    {
        $image = Image::open($this->getFullPath());

        if($image->width() > self::FULL_WIDTH)
        {
            $image->cropResize(self::FULL_WIDTH);
        }

        if($image->height() > self::FULL_HEIGHT)
        {
            $image->cropResize(self::FULL_HEIGHT);
        }

        $image->save($this->getFullPath(), 'png'); // Save resized original (full sized)

        $image->zoomCrop(self::THUMB_WIDTH, self::THUMB_HEIGHT);

        $image->save($this->getThumbnailDirectory().'/'.$this->getCleanFilename(), 'png'); // Save resized thumbnail (thumb sized)
    }
}
