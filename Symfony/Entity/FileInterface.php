<?php
/**
 * Created by PhpStorm.
 * User: tomaz
 * Date: 13/03/14
 * Time: 15:05
 */

namespace Service\Bundle\BaseBundle\Entity\Interfaces;

interface FileInterface {

    /**
     * Method must return a path, relative from web root
     * Example: files/account/1
     * @return string
     */
    public function getUploadDirectory();

    /**
     * @return array
     */
    public function getAllowedMimeTypes();

}