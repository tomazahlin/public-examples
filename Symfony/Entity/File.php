<?php
/**
 * Created by PhpStorm.
 * User: Tomaz
 * Date: 17.12.2013
 * Time: 13:38
 */

namespace Service\Bundle\BaseBundle\Entity;

use Service\Bundle\BaseBundle\Entity\Interfaces\FileInterface;
use Service\Bundle\BaseBundle\Helper\Symfony;
use Service\Bundle\BaseBundle\Helper\Text;
use Symfony\Component\HttpFoundation\File\Exception\UploadException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/* For every type of file (logo,image,document,attachment...) make a new class and extend this class */
/* Also implement the interface on the extended files */

abstract class File implements FileInterface
{
    /**
     * @var string
     */
    protected $path;

    /**
     * @var string
     */
    protected $directory;

    /**
     * @var UploadedFile
     */
    protected $file;

    /**
     * @var string
     */
    protected $cleanFilename;

    /**
     * @var string
     */
    protected $cleanBasename;

    /**
     * @var string
     */
    protected $extension;

    /**
     * Sets file.
     * @param UploadedFile $file
     * @return $this
     */
    public function setFile(UploadedFile $file)
    {
        $this->file = $file;
        $this->extension = $file->getClientOriginalExtension();

        $this->cleanBasename = Text::generateRandom(100);
        $this->cleanFilename = $this->cleanBasename .'.'.$this->extension;
        return $this;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @return string
     */
    public function getCleanFilename()
    {
        return $this->cleanFilename;
    }

    /**
     * @return string
     */
    public function getCleanBasename()
    {
        return $this->cleanBasename;
    }

    /**
     * @return string
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * @return null|string
     */
    public function getMimeType()
    {
        return $this->getFile()->getMimeType();
    }

    /**
     * @return string
     */
    public function getFullDirectory()
    {
        return Symfony::getWebFolder().$this->getUploadDirectory();
    }

    /**
     * @return string
     */
    public function getFullPath()
    {
        return $this->getFullDirectory().'/'.$this->getCleanFilename();
    }

    /**
     * @return string
     */
    public function getPath() {
        return $this->path;
    }

    /**
     * @throws UploadException
     * @return File
     */
    public function upload()
    {
        if (null === $this->getFile()) {
            throw new UploadException('File was not uploaded (null).');
        }
        elseif (!in_array($this->getFile()->getMimeType(),$this->getAllowedMimeTypes())) {
            throw new UploadException('File was not uploaded (Mime-Type not allowed).');
        }
        elseif($this->getFile()->move($this->getFullDirectory(),$this->getCleanFilename())) {
            $this->postUpload();
            return $this;
        }
        else
        {
            // Error uploading file to the server
            throw new UploadException('File was not uploaded due to an unknown error.');
        }
    }

    protected function postUpload()
    {
        // Does nothing. Implement this method in classes to define custom post upload execution
    }
}