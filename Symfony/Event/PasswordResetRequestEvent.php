<?php
/**
 * Created by PhpStorm.
 * User: tomaz
 * Date: 13/03/14
 * Time: 13:41
 */

namespace Service\Bundle\UserBundle\Event;

use Service\Bundle\UserBundle\Entity\User;

use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpFoundation\Request;

class PasswordResetRequestEvent extends Event {

    private $user;

    private $url;

    private $request;

    public function __construct(User $user, $url, Request $request)
    {
        $this->user = $user;
        $this->url = $url;
        $this->request = $request;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this->request;
    }

}