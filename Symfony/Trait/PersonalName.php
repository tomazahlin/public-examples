<?php
/**
 * Created by PhpStorm.
 * User: tomaz
 * Date: 14/03/14
 * Time: 17:52
 */

namespace Service\Bundle\BaseBundle\Entity\Traits;

trait PersonalName
{
    /**
     * @var string
     */
    protected $firstName;

    /**
     * @var string
     */
    protected $lastName;

    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getFirstName()
    {
        return $this->firstName;
    }

    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getLastName()
    {
        return $this->lastName;
    }

    public function getFullName()
    {
        return $this->getFirstName().' '.$this->getLastName();
    }
}