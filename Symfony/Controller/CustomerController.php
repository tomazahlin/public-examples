<?php

namespace Service\Bundle\CustomerBundle\Controller;

use Service\Bundle\BaseBundle\Controller\HelperController;
use Service\Bundle\CustomerBundle\Entity\Customer;
use Service\Bundle\CustomerBundle\Form\Type\CustomerType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\SecurityContext;

class CustomerController extends HelperController
{
    /**
     * New customer
     */
    public function newAction()
    {
        $this->getSecurity()->checkCustomerPermission(null, 'persist');
        $customer = new Customer();
        $account = $this->getAccount();

        if($this->getFormHandler()->process(new CustomerType(), $customer))
        {
            $customer->setAccount($account);
            $this->getEntityManager()->process('persist', $customer);
            return $this->serializeCreated($customer->getId());
        }
        return $this->serializeFormError();
    }

    /**
     * Update customer
     */
    public function updateAction($id)
    {
        $customer = $this->getCustomerRepository()->loadOne('id', $id);
        $this->getSecurity()->checkCustomerPermission($customer, 'persist');

        if($this->getFormHandler()->process(new CustomerType(), $customer))
        {
            $this->getEntityManager()->process('persist', $customer);
            return $this->okResponse();
        }
        return $this->serializeFormError();
    }

    /**
     * Get customer
     */
    public function getAction($id)
    {
        $customer = $this->getCustomerRepository()->loadOne('id', $id);
        $this->getSecurity()->checkCustomerPermission($customer, 'view');
        return $this->serialize($customer);
    }

    /**
     * Get customers
     */
    public function manyAction(Request $request)
    {
        $this->getSecurity()->checkCustomerPermission(null, 'list');
        $customers = $this->getCustomerRepository()->loadFiltered($request->query->all());
        return $this->serialize($customers);
    }

    /**
     * Get repairs for customer
     */
    public function repairsAction($id)
    {
        $customer = $this->getCustomerRepository()->loadOne('id', $id);
        $this->getSecurity()->checkCustomerPermission($customer, 'view');
        return $this->serialize($customer->getRepairs());
    }

    /**
     * Get items for customer
     */
    public function itemsAction($id)
    {
        $customer = $this->getCustomerRepository()->loadOne('id', $id);
        $this->getSecurity()->checkCustomerPermission($customer, 'view');
        return $this->serialize($customer->getItems());
    }

    /**
     * Remove customer
     */
    public function removeAction($id)
    {
        $customer = $this->getCustomerRepository()->loadOne('id', $id);
        $this->getSecurity()->checkCustomerPermission($customer, 'remove');
        $this->getEntityManager()->process('remove', $customer);
        return $this->okResponse();

    }

    /**
     * Find customer
     */
    public function findAction(Request $request)
    {
        $this->getSecurity()->checkCustomerPermission(null, 'find');
        $query = $request->query;

        if(!$query->has('term'))
        {
            $this->badRequest($this->trans('Missing term parameter!'));
        }

        $term = $query->get('term');
        $customers = $this->getCustomerRepository()->loadByLike($term);
        return $this->serialize($customers);

    }
}