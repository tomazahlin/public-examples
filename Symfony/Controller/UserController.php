<?php

namespace Service\Bundle\UserBundle\Controller;

use Service\Bundle\BaseBundle\Controller\HelperController;
use Service\Bundle\UserBundle\Entity\User;
use Service\Bundle\UserBundle\Event\PasswordResetEvent;
use Service\Bundle\UserBundle\Event\PasswordResetRequestEvent;
use Service\Bundle\UserBundle\Form\Type\PasswordResetRequestType;
use Service\Bundle\UserBundle\Form\Type\PasswordResetType;
use Service\Bundle\UserBundle\Form\Type\UserType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\SecurityContext;

class UserController extends HelperController
{
    /**
     * Request reset password
     */
    public function resetRequestAction(Request $request)
    {
        if($this->getFormHandler()->process(new PasswordResetRequestType(), new User()))
        {
            $email = $this->getFormHandler()->getForm('email')->getData();
            $user = $this->getUserRepository()->loadLogin($email);

            if($user === null) { # Wrong e-mail
                return $this->forbiddenResponse();
            }

            $user->generatePasswordResetToken();
            $this->getEntityManager()->process('persist', $user);

            $url = $this->container->get('service.angular_router')->generateUrl('reset/' . $user->getPasswordResetToken());

            $event = new PasswordResetRequestEvent($user,$url,$request);
            $this->dispatch('service.password_reset_request', $event);

            return $this->okResponse();
        }

        return $this->serializeFormError();
    }

    /**
     * Reset password
     */
    public function resetAction(Request $request, $token)
    {
        $user = $this->getUserRepository()->loadOne('passwordResetToken', $token);

        if($this->getFormHandler()->process(new PasswordResetType(), $user))
        {
            $this->getEntityManager()->process('persist', $user);

            $event = new PasswordResetEvent($user);
            $this->dispatch('service.password_reset', $event);

            return $this->okResponse();
        }


        if($request->getMethod() === 'GET' && $user instanceof User)
        {
            return $this->okResponse();
        }

        return $this->serializeFormError();
    }

    /**
     * Cancel reset password (removes token)
     */
    public function cancelResetAction($token)
    {
        $user = $this->getUserRepository()->loadOne('passwordResetToken', $token, false);

        if($user instanceof User) # Token found
        {
            $user->removePasswordResetToken();
            $this->getEntityManager()->process('persist', $user);

            return $this->okResponse();
        }
        else # Invalid token
        {
            return $this->forbiddenResponse();
        }
    }

    /**
     * Get user
     */
    public function getAction($id)
    {
        $user = $this->getUserRepository()->loadOne('id', $id, true, true);
        $this->getSecurity()->checkUserPermission($user, 'view');
        return $this->serialize($user);
    }

    /**
     * Get users
     */
    public function manyAction(Request $request)
    {
        $this->getSecurity()->checkUserPermission(null, 'list');
        $get = $request->query;
        $users = $this->getUserRepository()->loadFiltered($get->all(), false);
        return $this->serialize($users);
    }

    /**
     * Get following repairs for user
     */
    public function followingRepairsAction($id)
    {
        $user = $this->getUserRepository()->loadOne('id', $id, true, true);
        $this->getSecurity()->checkUserPermission($user, 'view');
        return $this->serialize($user->getFollowingRepairs());
    }

    /**
     * Get locations for user
     */
    public function locationsAction($id)
    {
        $user = $this->getUserRepository()->loadOne('id', $id, true, true);
        $this->getSecurity()->checkUserPermission($user, 'view');
        return $this->serialize($user->getLocations());
    }

    /**
     * Get profile
     */
    public function profileAction()
    {
        $user = $this->getUser();
        return $this->serialize($user);
    }

    /**
     * Get profile primary location
     */
    public function primaryLocationAction($id)
    {
        $user = $this->getUserRepository()->loadOne('id', $id, true, true);
        $this->getSecurity()->checkUserPermission($user, 'view');
        $location = $user->getPrimaryLocation();
        return $this->serialize($location);
    }

    /**
     * Update user
     */
    public function updateAction($id)
    {
        $user = $this->getUserRepository()->loadOne('id', $id, true, true);
        $this->getSecurity()->checkUserPermission($user, 'persist');

        $user === $this->getSecurity()->getUser() ? $subtype = 'profile' : $subtype = 'manager';

        if($this->getFormHandler()->process(new UserType(), $user, array('subtype' => $subtype)))
        {
            $this->getEntityManager()->process('persist', $user);
            return $this->okResponse();
        }
        return $this->serializeOrRenderForm('ServiceUserBundle:User');
    }

    /**
     * Logout (removes token)
     */
    public function logoutAction(Request $request)
    {
        $user = $this->getSecurity()->getUser();

        $tokenValue = $request->headers->get('X-Auth-Token'); # Is always present when accessing logout through api

        if($user instanceof User && $user->removeTokenValue($tokenValue))
        {
            $this->getEntityManager()->process('persist', $user);
            return $this->okResponse('Logout successful.');
        }
        return $this->forbiddenResponse('Logout failed.');
    }

    /**
     * Credit cards
     */
    public function creditCardsAction($id)
    {
        $user = $this->getUserRepository()->loadOne('id', $id, true, true);
        $this->getSecurity()->checkUserPermission($user, 'view');
        $logs = $this->container->get('service.stripe')->getCreditCards($user);
        return $this->serialize($logs);
    }

    /**
     * Logs
     */
    public function logsAction(Request $request, $id)
    {
        $user = $this->getUserRepository()->loadOne('id', $id, true, true);
        $this->getSecurity()->checkUserPermission($user, 'cards');
        return $this->serialize($user->getLogs($request));
    }

    /**
     * Remove credit card
     */
    public function removeCreditCardAction($id, $stripeCardId)
    {
        $user = $this->getUserRepository()->loadOne('id', $id, true, true);
        $this->getSecurity()->checkUserPermission($user, 'cards');

        if($this->container->get('service.stripe')->removeCreditCard($user, $stripeCardId))
        {
            return $this->okResponse();
        }
        return $this->forbiddenResponse();
    }

    /**
     * Email availability
     */
    public function checkEmailAction(Request $request)
    {
        $email = $request->request->get('email');
        return $this->serialize(['available' => $this->getUserRepository()->isEmailAvailable($email)]);
    }

    /**
     * Find user
     */
    public function findAction(Request $request)
    {
        $this->getSecurity()->checkUserPermission(null, 'find');
        $query = $request->query;

        if(!$query->has('term'))
        {
            $this->badRequest($this->trans('Missing term parameter!'));
        }

        $term = $query->get('term');
        $users = $this->getUserRepository()->loadByLike($term, true);
        return $this->serialize($users);

    }
}
