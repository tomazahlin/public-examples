<?php

namespace Service\Bundle\UserBundle\Controller;

use Service\Bundle\BaseBundle\Controller\HelperController;
use Service\Bundle\UserBundle\Entity\UserLog;
use Service\Bundle\UserBundle\Entity\Account;
use Service\Bundle\UserBundle\Entity\InvitationActivation;
use Service\Bundle\UserBundle\Entity\Location;
use Service\Bundle\UserBundle\Entity\Registration;
use Service\Bundle\UserBundle\Entity\RegistrationActivation;
use Service\Bundle\UserBundle\Entity\User;
use Service\Bundle\UserBundle\Event\InvitationActivationEvent;
use Service\Bundle\UserBundle\Event\RegistrationEvent;
use Service\Bundle\UserBundle\Event\RegistrationPostActivationEvent;
use Service\Bundle\UserBundle\Event\RegistrationPreActivationEvent;
use Service\Bundle\UserBundle\Form\Type\InvitationActivationType;
use Service\Bundle\UserBundle\Form\Type\InvitationType;
use Service\Bundle\UserBundle\Form\Type\RegistrationActivationType;
use Service\Bundle\UserBundle\Form\Type\RegistrationType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\SecurityContext;

class RegistrationController extends HelperController
{
    public function registerAction()
    {
        $user = new User();
        $account = new Account();
        $registration = new Registration($user,$account);

        if($this->getFormHandler()->process(new RegistrationType(), $registration))
        {
            $user->setMain(true);
            $account->addUser($user);
            $user->log(UserLog::REGISTER);
            $this->getEntityManager()->process('persist', $account);

            $event = new RegistrationEvent($account, $user);
            $this->dispatch('service.registration_email', $event);

            return $this->okResponse();
        }

        return $this->serializeFormError();
    }

    public function resendRegistrationEmailAction(Request $request)
    {
        if($request->request->has('email'))
        {
            $email = $request->request->get('email');
            $user = $this->getUserRepository()->loadOne('email', $email, false, false);

            if($user instanceof User)
            {
                $event = new RegistrationEvent($user->getAccount(), $user);
                $this->dispatch('service.registration_email', $event);
                return $this->okResponse();
            }
        }

        return $this->forbiddenResponse();
    }

    public function activateAction(Request $request, $token)
    {
        $user = $this->getUserRepository()->loadOne('activationToken', $token);
        $account = $user->getAccount();

        $success = false;

        if($user->isMain()) # First user which creates account also has to enter account information
        {
            $location = new Location();
            $group = $this->getGroupRepository()->loadOne('alias', 'owner');
            $user->setGroup($group);

            $activation = new RegistrationActivation($user,$account,$location);

            if($this->getFormHandler()->process(new RegistrationActivationType(), $activation))
            {
                $activation->getLocation()
                                ->enable();

                $activation->getUser()->setPrimaryLocation($activation->getLocation())
                     ->activate()
                     ->removeActivationToken()
                     ->log(UserLog::ACTIVATE);

                $token = $activation->getUser()->generateToken();

                $account = $activation->getAccount();

                $packageDuration = $this->getPackageDurationRepository()->loadOneByAliasAndDuration('subscription', 1);

                $event = new RegistrationPreActivationEvent($account, $packageDuration);
                $this->dispatch('service.activation_pre_persist', $event);

                $account->addGroup($group,false) # Here we do not want the first group to have bidirectional relationship (it is general group)
                        ->activate()
                        ->enable();

                $this->getEntityManager()->process('persist', $account);

                $event = new RegistrationPostActivationEvent($account);
                $this->dispatch('service.activation_post_persist', $event);
                $success = true;
            }
        }
        else # Invited user
        {
            if($this->getFormHandler()->process(new InvitationActivationType(), new InvitationActivation($user)))
            {
                $user->activate()
                     ->removeActivationToken()
                     ->log(UserLog::ACTIVATE);

                $token = $user->generateToken();

                $this->getEntityManager()->process('persist', $user);

                $event = new InvitationActivationEvent($user);
                $this->dispatch('service.invitation_activation', $event);
                $success = true;
            }
        }

        if($success)
        { # Send the token and data, to automatically log-in the user
            $data = ['token' => $token->getValue(),
                    'userId' => $user->getId(),
                    'accountId' => $user->getAccount()->getId(),
                    'roles' => $user->getGroup()->getRoles()];
            return $this->serialize($data);
        }

        if($request->getMethod() === 'GET')
        {
            return $this->serialize([
                'user' => array('main' => $user->isMain()),
                'account' => array('name' => $account->getName())]
            );
        }

        return $this->serializeFormError();
    }

    /**
     * Invite new user to the app
     */
    public function inviteAction()
    {
        $this->getSecurity()->checkUserPermission(null, 'invite');

        $account = $this->getUser()->getAccount();

        $user = new User();
        $user->setAccount($account)->setMain(false);

        if($this->getFormHandler()->process(new InvitationType(), $user))
        {
            $user->log(UserLog::INVITE);
            $this->getEntityManager()->process('persist', $user);

            $event = new RegistrationEvent($account, $user);
            $this->dispatch('service.invitation', $event);

            return $this->serializeCreated($user->getId());
        }

        return $this->serializeFormError();
    }
}
